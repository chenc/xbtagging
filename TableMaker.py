import toolkit
import math,re,os,commands

from copy import copy,deepcopy

version = 'default'

class BreakDown(toolkit.IOHandler):
    label = 1
    def __init__(self,output_path,uncertainties_version,variable=['Scale factor','sf']):
        self.uncertainties = GroupSystmatics().GetUncertainties(uncertainties_version)
        self.output_path = output_path
        self.head = variable[0]
        self.key = variable[1]
        #table size
        self.config_default = { 
            'width':270,
            'height':270,
            't_b_l_rMargin':[2,2,2,2],
            'scaleX':1.5,
            'scaleY':1.5,
        }
        if not os.path.isdir(self.output_path):
            os.system('mkdir -p %s'%(self.output_path))

    def _sum2(self,entries):
        N = len(entries[0])
        res = [0.]*N
        for entry in entries:
            for n in range(N):
                res[n] += entry[n]**2
        for n in range(N):
            res[n] = math.sqrt(res[n])            
        return res
    
    def GetTexAll(self,tex_name,input_json,intervals,title,readPickle=False):
        entries = self._getEntries(input_json,readPickle)

        name = tex_name + '_group_standalone'
        self._getTex({'height':270,'scaleX':1.3,'scaleY':1.3},name,entries,intervals,title,isGroup=True,isStandalone=True)
        name = tex_name + '_detail_standalone'
        self._getTex({'height':600,'scaleX':1.2,'scaleY':1.0},name,entries,intervals,title,isGroup=False,isStandalone=True)
        name = tex_name + '_group'
        self._getTex({'scaleX':0.8,'scaleY':0.8},name,entries,intervals,title,isGroup=True,isStandalone=False)


    def _addEntry(self,tab_list,entries,isGroup=True,isPercentage=False,entries_alter=None):
        tot = entries[self.head]
        nbins = len(tot)
        if isPercentage:
            fmt = lambda errs:list(map(lambda x,y:'{0:.1f}'.format(100.*(x/y)),errs,tot))
        else:
            fmt = lambda errs:list(map(lambda x:'{0:.3f}'.format(x),errs))

        def AppendSys(name,entry):
            if entry == None:
                content = [name] + ['-']*nbins
            else:
                content = [name] + fmt(entry)
            content_str = ' & '.join(content) + r'\\'
            tab_list.append(content_str)

        def AddEntry(name):
            if not name in entries: #systs not known
                self.FBIWarning('{0:} not even registered !'.format(name))
                AppendSys(name,None)
            elif isinstance(entries[name],list): # nominal/stat/syst/total
                AppendSys(name,entries[name]) 
            elif len(entries[name]) == 0: # empty systs
                if isGroup or len(self.uncertainties[name])==1:
                    AppendSys(name,None)
                else:
                    for itm in self.uncertainties[name]:
                        AppendSys(itm,None)
            elif len(self.uncertainties[name]) == 1: # single systs
                AppendSys(name,entries[name].values()[0])
            else: # multiple systs
                if not isGroup:
                    for itm in self.uncertainties[name]:
                        itm_regular = re.sub('_','\_',itm)
                        entry = entries[name][itm] if itm in entries[name] else None                    
                        AppendSys(itm_regular,entry)
                else:
                    init = [0.] * nbins
                    entry = self._sum2(entries[name].values() + [init])    
                    AppendSys(name,entry)
        return AddEntry

    def _getEntries(self,input_json,readPickle=False):
        def WrapGetter(data):
            def getter(key):
                if readPickle:
                    entry = toolkit.Searcher(data,nameMap)(key)
                else:
                    entry = data.get(key)
                return entry
            return getter

        nameMap = GroupSystmatics().MapPickle()
        data = WrapGetter(toolkit.json_load(input_json))(self.key)
        getter = WrapGetter(data)
        nominal = getter('nominal')
        stat = getter('data stats')
        systs = []
        entries = {}
        for name, uncertainty_group in self.uncertainties.iteritems():
            group = {}
            for item in uncertainty_group:
                entry = getter(item)
                if not entry:
                    self.Warning('skip {0:}'.format(item))                    
                else:
                    group[item] = entry
                    systs.append(entry)
            entries[name] = group
        syst_sum = self._sum2(systs)
        total = self._sum2([syst_sum,stat])
        entries[self.head] = nominal
        entries[r'Statistical uncertainty'] = stat
        entries[r'Systematic uncertainty'] = syst_sum
        entries[r'Total uncertainty'] = total
        return entries

    def _maketabularList(self,entries,tabular_list,addEntry,addEntryP,intervals):
        nBins = len(entries[self.head])+1
        tabular_list.append( r'\begin{tabular}{l'+r'|r'*(nBins-1)+r'}' )
        tabular_list.append(r'\hline')
        tabular_list.append(r'\hline')
        tabular_list.append(r'\multicolumn{'+str(nBins)+r'}{c}{T\&P Method} \\')
        tabular_list.append(r'\hline')
        tabular_list.append(r'\hline')
        tabular_list.append(intervals)
        tabular_list.append(r'\hline')
        tabular_list.append(r'\hline')
        addEntry(self.head)
        addEntry(r'Total uncertainty')
        addEntry(r'Statistical uncertainty')
        addEntry(r'Systematic uncertainty')
        tabular_list.append(r'\hline')
        tabular_list.append(r'\hline')
        tabular_list.append(r'\multicolumn{'+str(nBins)+r'}{c}{Systematic Uncertainties [\%]} \\')
        tabular_list.append(r'\hline')
        tabular_list.append(r'\hline')
        addEntryP(r'Matrix element modelling ($t\bar{t}$)')
        addEntryP(r'Parton shower / Hadronisation ($t\bar{t}$)')
        addEntryP(r'PDF reweighting ($t\bar{t}$)')
        addEntryP(r'PDF reweighting (single top)')
        addEntryP(r'More / less radiation ($t\bar{t}$)')
        addEntryP(r'More / less radiation (single top)')
        addEntryP(r'DR vs. DS (single top)')
        addEntryP(r'Modelling (Z+jets)')
        addEntryP(r'Modelling (diboson)')
        tabular_list.append(r'\hline')
        addEntryP(r'Limited size of simulated samples')
        tabular_list.append(r'\hline')
        addEntryP(r'Normalisation single top')
        addEntryP(r'Normalisation Z+jets')        
        addEntryP(r'Normalisation diboson')
        addEntryP(r'Normalisation misid. leptons')
        tabular_list.append(r'\hline')

        addEntryP(r'Pile-up reweighting')
        addEntryP(r'Electron efficiency/resolution/scale/trigger')
        addEntryP(r'Muon efficiency/resolution/scale/trigger')
        addEntryP(r'$E_{T}^{miss}$')
        addEntryP(r'JVT')
        addEntryP(r'Jet energy scale (JES)')
        addEntryP(r'Jet energy resolution (JER)')
        addEntryP(r'Luminosity (3.2\%)')
        tabular_list.append(r'\hline')
        tabular_list.append(r'\bottomrule')
        tabular_list.append(r'\end{tabular}')
        return tabular_list
        
    def _wrapTableStandalone(self,config,tabular_list,title,table_number):
        tabular_list = copy(tabular_list)
        tabular_list.insert(0,r'\scalebox{{{0:}}}[{1:}]{{'.format(config['scaleX'],config['scaleY']))        
        tabular_list.append(r'}')
        
        tabular_list.insert(0,r'\begin{table}[htbp]') 
        tabular_list.insert(1,r'\renewcommand\thetable{'+str(table_number)+'}')
        tabular_list.insert(2,r'\centering')
        
        tabular_list.append(r'\captionsetup{font=large}')
        tabular_list.append(r'\caption{'+title+'}')
        tabular_list.append(r'\end{table}')

        return tabular_list

    def _wrapTable(self,config,tabular_list,title,table_number):
        tabular_list = copy(tabular_list)
        tabular_list.insert(0,r'\scalebox{{{0:}}}[{1:}]{{'.format(config['scaleX'],config['scaleY']))        
        tabular_list.append(r'}')
        
        tabular_list.insert(0,r'\begin{table}[htbp]') 
        tabular_list.insert(1,r'\centering')
        
        tabular_list.append(r'\captionsetup{font=large}')
        tabular_list.append(r'\caption{'+title+'}')
        tabular_list.append(r'\end{table}')

        return tabular_list

    

    def _wrapDocument(self,table,config):
        table = copy(table)
        table.insert(0, r'\begin{document}')
        table.append(r'\end{document}')

        table.insert(0,r'\documentclass{article}')
        table.insert(1,r'\usepackage{booktabs}')
        table.insert(2,r'\usepackage{graphics}')
        table.insert(3,r'\usepackage{caption}')
        table.insert(4,r'\usepackage[paperwidth={0:}mm,paperheight={1:}mm]{{geometry}}'.format(config['width'],config['height']))
        table.insert(5,r'\geometry{{tmargin={0:}mm,bmargin={1:}mm,lmargin={2:}mm,rmargin={3:}mm}}'.format(*config['t_b_l_rMargin']))
        return table

    def _dumpTex(self,name,tabular_list):
        tex_path = '{0:}/{1:}.tex'.format(self.output_path,name)
        with open(tex_path,'w') as f:
            for line in tabular_list:
                f.write(line+'\n')
        return tex_path

    def _makePDF(self,input_tex):
        res = commands.getstatusoutput('pdflatex -halt-on-error -output-directory={0:} {1:}'.format(self.output_path,input_tex))
        if res[0]:
            raise  RuntimeError(res[1])
        else:
            commands.getstatusoutput('rm {0:}'.format(re.sub('.tex$','.log',input_tex)))
            commands.getstatusoutput('rm {0:}'.format(re.sub('.tex$','.aux',input_tex)))
            pdf_status = commands.getstatusoutput('ls {0:}'.format(re.sub('.tex$','.pdf',input_tex)))
            if not pdf_status[0]:
                self.Stdout('PDF file generated',pdf_status[1])
            else:
                raise RuntimeError('PDF not founed: {0:}'.format(pdf_status[1]))

    def _getInterval(self,interval_list):
        nbins = len(interval_list)-1
        interval_str_list = [r'$p_{T}$ Interval [GeV]']
        for n in range(nbins):
            interval_str_list.append('{0:}-{1:}'.format(interval_list[n],interval_list[n+1]))
        interval_str = ' & '.join(interval_str_list)
        interval_str += r'\\'
        return interval_str

    def _updateEntries(self,entriesA,entriesB):
        entries = deepcopy(entriesA)
        for name,items in self.uncertainties.iteritems():
            for itm in items:
                if not itm in entriesA[name]:
                    if itm in entriesB[name]:
                        entries[name][itm] = entriesB[name][itm]
        return entries

    def _getTex(self,_config,tex_name,entries,intervals,title,isGroup,isStandalone=True):
        config = deepcopy(self.config_default)
        config.update(_config)
        tabular_list = []
        addEntry = self._addEntry(tabular_list,entries,isGroup,isPercentage=False)
        addEntryP = self._addEntry(tabular_list,entries,isGroup,isPercentage=True)
        intervals = self._getInterval(intervals)
        tabular_list = self._maketabularList(entries,tabular_list,addEntry,addEntryP,intervals)

        if isStandalone:
            table = self._wrapTableStandalone(config,tabular_list,title,type(self).label)
            document = self._wrapDocument(table,config)
            tex_path = self._dumpTex(tex_name,document)
            self._makePDF(tex_path)
            type(self).label += 1
        else:
            table = self._wrapTable(config,tabular_list,title,type(self).label)            
            tex_path = self._dumpTex(tex_name,table)
            self.Stdout('Tex file (for note) generated',tex_path)

class GroupSystmatics(object):
    def __init__(self):
        pass

    def GetUncertainties(self,version):
        return self._getUncertainties()

    def _getUncertainties(self):
        res = {
            #mcstat
            r'Limited size of simulated samples':[
                'mc stats',
            ],

            #detector related
            r'Electron efficiency/resolution/scale/trigger':[
                'EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR',
                'EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR',
                'EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR',
                'EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR',
                'EG_RESOLUTION_ALL',
                'EG_SCALE_ALL',
            ],
            r'Jet energy resolution (JER)':[
                'JET_JER_SINGLE_NP',
            ],
            r'Jet energy scale (JES)':[
                'JET_23NP_JET_EffectiveNP_1',
                'JET_23NP_JET_EffectiveNP_2',
                'JET_23NP_JET_EffectiveNP_3',
                'JET_23NP_JET_EffectiveNP_4',
                'JET_23NP_JET_EffectiveNP_5',
                'JET_23NP_JET_EffectiveNP_6',
                'JET_23NP_JET_EffectiveNP_7',
                'JET_23NP_JET_EffectiveNP_8restTerm',
                'JET_23NP_JET_BJES_Response',
                'JET_23NP_JET_EtaIntercalibration_Modelling',
                'JET_23NP_JET_EtaIntercalibration_NonClosure_highE',
                'JET_23NP_JET_EtaIntercalibration_NonClosure_negEta',#TODO added
                'JET_23NP_JET_EtaIntercalibration_NonClosure_posEta',
                'JET_23NP_JET_EtaIntercalibration_TotalStat',
                'JET_23NP_JET_Flavor_Composition',
                'JET_23NP_JET_Pileup_OffsetMu',
                'JET_23NP_JET_Pileup_OffsetNPV',
                'JET_23NP_JET_Pileup_PtTerm',
                'JET_23NP_JET_Pileup_RhoTopology',
                'JET_23NP_JET_PunchThrough_MC16',
                'JET_23NP_JET_SingleParticle_HighPt',
            ],
            r'Muon efficiency/resolution/scale/trigger':[
                'MUON_EFF_ISO_STAT',
                'MUON_EFF_ISO_SYS',
                'MUON_EFF_RECO_STAT',
                'MUON_EFF_RECO_STAT_LOWPT',
                'MUON_EFF_RECO_SYS',
                'MUON_EFF_RECO_SYS_LOWPT',
                'MUON_EFF_TTVA_STAT',
                'MUON_EFF_TTVA_SYS',
                'MUON_EFF_TrigStatUncertainty',
                'MUON_EFF_TrigSystUncertainty',
                'MUON_ID',
                'MUON_MS',
                'MUON_SAGITTA_RESBIAS',
                'MUON_SAGITTA_RHO',
                'MUON_SCALE',

            ],
            r'Pile-up reweighting':[
                'PRW_DATASF',
            ],
            r'$E_{T}^{miss}$':[
                'METTrigStat',
                'METTrigSumpt',
                'METTrigTop',
                'METTrigZ',
                'MET_JetTrk_Scale',
                'MET_SoftTrk_Scale',
                'MET_SoftTrk_ResoPara',
                'MET_SoftTrk_ResoPerp',
            ],
            r'JVT':[
                'JET_JvtEfficiency'
            ],

            #modellings

            r'Parton shower / Hadronisation ($t\bar{t}$)':[
                'tt Fragmentation',
            ],
            r'More / less radiation ($t\bar{t}$)':[
                'tt Radiation High',
                'tt Radiation Low',
            ],
            r'Matrix element modelling ($t\bar{t}$)':[
                'tt HardScatter',
            ],
            r'More / less radiation (single top)':[
                'stop Radiation High',
                'stop Radiation Low',

            ],
            r'DR vs. DS (single top)':[
                'stop DRDS',
            ],

            r'Modelling (Z+jets)':[
                'zjets Modelling',
            ],

            r'Modelling (diboson)':[
                'diboson Modelling',
            ],            

            #normalization

            r'Normalisation single top':[
                'WtScale',
            ],

            r'Normalisation diboson':[
                'dibosonScale'
            ],

            r'Normalisation Z+jets':[
                'zjetsScale',
            ],

            r'Normalisation misid. leptons':[
                'fakeScale',
            ],
            
            r'Luminosity (3.2\%)':[
                'lumiScale',
            ],
        }
        res[r'PDF reweighting ($t\bar{t}$)'] = []
        res['PDF reweighting (single top)'] = []
        for n in range(1,31):
            res[r'PDF reweighting ($t\bar{t}$)'].append('tt PDF Error %s'%(n))
            res['PDF reweighting (single top)'].append('stop PDF Error %s'%(n))
        return res

    def MapPickle(self):
        NameMap = { 
            'mc_stat' : 'mc stats',
            'dt_stat' : 'data stats', 
            'FakeScale' : 'fakeScale',
            'Luminosity' : 'lumiScale',
            'norm diboson' : 'dibosonScale',
            'norm wt' : 'WtScale',
            'norm zjets': 'zjetsScale',            
            'mod_stop_StopDRDS' : 'stop DRDS',
            'mod_stop_StopFragmentation' : 'stop Fragmentation',
            'mod_stop_StopRadiation' : 'stop Radiation',
            'mod_tt_ttbarFragmentation' : 'tt Fragmentation',
            'mod_tt_ttbarHardScatter' : 'tt HardScatter',
            'mod_tt_ttbarPDFRW' : 'tt PDFRW',
            'mod_tt_ttbarRadiation' : 'tt Radiation',
            'mod_z+jets_zjetsModelling' : 'zjets Modelling',
        }
        return NameMap

@toolkit.TimeCalculator()
def run_table(period,tagger,taggerCut,taggerEff,mvaWP):
    outputPath = './output.{0:}/{1:}/{2:}_{3:}_MVA{4:}/'.format(version,period,tagger,taggerCut,mvaWP)
    resultJson = '{0:}/result_{1:}.json'.format(outputPath,taggerEff)

    
    tableName = 'breakdown_%s'%(taggerEff)
    
    intervals = [20,30,40,60,85,110,140,175,250,600]
    titleMap = {
        'Data1516':'data1516',
        'Data17':'data17',
        'Data151617':'data15-17',
        'Data18':'data18',
        'Data15161718':'data15-18',
        'FixedCut':'fixed cut',
        'Hyb':'hybrid cut',
        100:'without',
        80:'with',
    }

    title = 'Calibration results for {0:}, {1:}\% {2:} working point of {3:} tagger used, {4:} purity BDT cut implemented'.format(titleMap[period],taggerEff,titleMap[taggerCut],tagger,titleMap[int(mvaWP)])

    maker = BreakDown(outputPath, period)
    maker.GetTexAll(tableName, resultJson, intervals, title)


def main():
    period = 'mc16a'
    tagger = 'MV2c10'
    taggerCut = 'FixedCut'
    taggerEff = '77'

    run_table(period,tagger,taggerCut,taggerEff,100)
    run_table(period,tagger,taggerCut,taggerEff,80)

if __name__ == '__main__':
    main()
