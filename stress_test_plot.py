import AtlasStyle
import ROOT as R

from array import array
import os,json,math
version = 'default'
R.gROOT.SetBatch(1)
class DrawStress(object):
    def __init__(self,outputPath,nameA,nameB):
        self.outputPath = outputPath
        if not os.path.isdir(outputPath):
            os.system('mkdir -p %s'%(outputPath))
        self.intervals = [20,30,40,60,85,110,140,175,250,600] 
        self.nameA = nameA
        self.nameB = nameB

    def Draw(self,name,resultJson):
        data = self.fetchData(resultJson)
        objects = self.GetObjects(data['entryA'],data['entryB'],data['stat'])
        self.DrawObjects(name,objects)

    def fetchData(self,resultJson):
        def divC(x,y):
            res = 0. if y==0. else (x-y)/y
            return res
        def divE(x,y):
            res = 0. if y==0. else x/y
            return res
        
        res = {}

        data = json.load(open(resultJson,'r'))
        
        truth = data['e_dt']['result truth']

        stat = data['e_dt']['data stats']
        res['stat'] = map(divE, stat,truth)

        central_A = data['e_dt']['result %s'%(self.nameA)]
        mc_stat_A = data['e_dt']['mc stats %s'%(self.nameA)]
        total_A = self.fetchError(data, self.nameA, 'e_dt')

        central_B = data['e_dt']['result %s'%(self.nameB)]
        mc_stat_B = data['e_dt']['mc stats %s'%(self.nameB)]
        total_B = self.fetchError(data, self.nameB, 'e_dt')


        res['entryA'] = {}
        res['entryA']['central'] = map(divC, central_A,truth)
        res['entryA']['mc stat'] = map(divE, mc_stat_A,truth)
        res['entryA']['total'] = map(divE, total_A,truth)

        res['entryB'] = {}
        res['entryB']['central'] = map(divC, central_B,truth)
        res['entryB']['mc stat'] = map(divE, mc_stat_B,truth)
        res['entryB']['total'] = map(divE, total_B,truth)

        return res

    def fetchError(self,data,name,key):
        error = [0.] * len(data[key]['result %s'%(name)])
        for k,v in data[key].iteritems():
            if k.startswith('modelling tt') or k.startswith('mc stats'):
                for n in range(len(error)):
                    error[n] = math.sqrt(error[n]**2+v[n]**2)
        return error


    def GetObjects(self,entryA,entryB,stat):

        res = {}
        res['canvas'] = R.TCanvas('c1','c1',800,800)        
        res['boxA'] = self.GetBoxA(entryA['central'], entryA['mc stat'])
        res['boxB'] = self.GetBoxB(entryB['central'], entryB['mc stat'])
        res['barA'] = self.GetBarA(entryA['central'], entryA['total'])
        res['barB'] = self.GetBarB(entryB['central'], entryB['total'])
        res['band'] = self.GetBand(stat)
        res['line'] = self.GetLineZero()
        res['legend'] = self.GetLegend(res['band'], res['barA'], res['barB'])
        res['multi_graph'] = self.GetMultiGraph(res['boxA'],res['boxB'],res['band'],res['line'])
        return res
    
    def GetMultiGraph(self,boxA,boxB,band,line):
        multi_graph = R.TMultiGraph('multi_graph','multi_graph')
        multi_graph.Add(band)
        multi_graph.Add(boxA)
        multi_graph.Add(boxB)
        multi_graph.Add(line,'L')

        multi_graph.GetXaxis().SetTitle('Jet p_{T} [GeV]')
        multi_graph.GetXaxis().SetNdivisions(205)
        multi_graph.GetXaxis().SetLabelSize(0.035)
        multi_graph.GetXaxis().SetTitleOffset(1.3)
        multi_graph.GetXaxis().SetTitleSize(0.04)
        multi_graph.GetYaxis().SetTitle('(#epsilon_{b,Estimated}-#epsilon_{b,True}) / #epsilon_{b,True}')
        multi_graph.GetYaxis().SetNdivisions(507)

        for n in range(len(self.intervals)-1):
            x = multi_graph.GetXaxis().FindBin(n*100+50)
            multi_graph.GetXaxis().SetBinLabel(x,'%s-%s'%(self.intervals[n],self.intervals[n+1]))
        multi_graph.GetXaxis().LabelsOption('h')
        multi_graph.GetYaxis().SetRangeUser(-0.1,0.1)
        multi_graph.GetYaxis().SetLabelSize(0.03)        
        return multi_graph

    def GetBarA(self,central,total):
        graphError = self.getGraphError(central, total, shift=-1)
        graphError.SetLineColor(R.kBlack)
        graphError.SetMarkerColor(R.kBlack)
        graphError.SetLineWidth(3)        
        return graphError

    def GetBarB(self,central,total):
        graphError = self.getGraphError(central,total,shift=1)
        graphError.SetLineColor(R.kRed-3)
        graphError.SetMarkerColor(R.kRed-3)
        graphError.SetLineWidth(3)        
        return graphError

    def GetBoxA(self,central,mc_stat):
        graphError = self.getGraphError(central, mc_stat, shift=-1,xErrorType=2)
        graphError.SetLineColor(R.kBlack)
        graphError.SetMarkerSize(1.)
        graphError.SetLineWidth(3)
        graphError.SetFillStyle(0)
        return graphError

    def GetBoxB(self,central,mc_stat):
        graphError = self.getGraphError(central, mc_stat, shift=1,xErrorType=2)
        graphError.SetLineColor(R.kRed-3)
        graphError.SetMarkerSize(1.)
        graphError.SetLineWidth(3)
        graphError.SetFillStyle(0)
        return graphError

    def GetBand(self,data_stat):
        central = [0. for _ in data_stat]
        res = self.getGraphError(central, data_stat, 0.,1)
        res.SetMarkerSize(1);
        res.SetFillColor(R.kGreen-8)
        res.SetLineColor(R.kGreen-8) 
        res.SetFillStyle(1001)
        res.SetLineWidth(2)       
        return res

    def GetLineZero(self):
        xs = array('d',[0,900])
        ys = array('d',[0.,0.])
        line = R.TGraph(2,xs,ys)
        line.SetLineColor(R.kBlack)
        line.SetLineWidth(2)
        line.SetLineStyle(2)        
        return line

    def DrawObjects(self,name,objects):

        objects['canvas'].Draw()
        
        objects['multi_graph'].Draw('A5')

        objects['barA'].Draw('PSAME')
        
        objects['barB'].Draw('PSAME')

        objects['legend'].Draw()
        
        objects['canvas'].Print('%s/%s.png'%(self.outputPath,name))
    
    


    def GetLegend(self,band,barA,barB):
        self.dummyObjs = {}
        self.dummyObjs['mcStat'] = R.TH1F()
        self.dummyObjs['mcStat'].SetFillColor(0)
        self.dummyObjs['mcStat'].SetLineColor(18)

        self.dummyObjs['tot'] = R.TH1F()
        self.dummyObjs['tot'].SetLineColor(18)
        self.dummyObjs['tot'].SetMarkerColor(18)

        legend = R.TLegend(0.325,0.17,0.75,0.35)
        legend.AddEntry(band,'data (pseudo) stat unc.','f')        
        legend.AddEntry(self.dummyObjs['mcStat'],'MC stat unc.','f')
        legend.AddEntry(self.dummyObjs['tot'],'MC stat #oplus t#bar{t} modelling unc.','lep')
        legend.AddEntry(barA,self.nameA,'pl')
        legend.AddEntry(barB,self.nameB,'pl')
        legend.SetBorderSize(0)
        legend.SetFillStyle(0)
        return legend

    def getGraphError(self,central,error,shift,xErrorType=0):
        xs = []
        ys = []
        ex = []
        ey = []
        N = len(self.intervals)-1
        for n in range(N):
            xs.append(self.getX(n,shift))
            ys.append(central[n])
            ey.append(error[n])
            ex.append(self.getEX(n,xErrorType))

        xs,ys,ex,ey = map(lambda x:array('d',x),[xs,ys,ex,ey])

        graph = R.TGraphErrors(N,xs,ys,ex,ey)        

        return graph

    def getX(self,n,shift):
        return n*100 + 50. + shift*10.

    def getEX(self,n,xErrorType):
        if xErrorType==1:
            return 50.
        elif xErrorType==0:
            return 0.
        elif xErrorType==2:
            return 10
        else:
            raise ValueError('xErrorType = 0,1,2, %s found'%(xErrorType))

def run_stress_test_plot(period,tagger,taggerCut,taggerEff,mvaWP):
    outputPath = './output.{0:}/{1:}/{2:}_{3:}_MVA{4:}/'.format(version,period,tagger,taggerCut,mvaWP)    
    resultJson = '{0:}/stress_result_{1:}.json'.format(outputPath,taggerEff)
    
    nameA = 'PwPy8'
    nameB = 'PowhegHerwig'
    name = 'stress_plot_%s'%(taggerEff)    
    drawer = DrawStress(outputPath, nameA, nameB)
    drawer.Draw(name,resultJson)

def run_stress_test_plot_closure(period,tagger,taggerCut,taggerEff,mvaWP):
    outputPath = './output.{0:}/{1:}/{2:}_{3:}_MVA{4:}/'.format(version,period,tagger,taggerCut,mvaWP)
    resultJson = '{0:}/stress_result_{1:}.json'.format(outputPath,taggerEff)
    
    nameA = 'PwPy8'
    nameB = 'sherpa'
    name = 'stress_plot_closure_%s'%(taggerEff)    
    drawer = DrawStress(outputPath, nameA, nameB)
    drawer.Draw(name,resultJson)

def main():
    pass

if __name__ == '__main__':
    main()