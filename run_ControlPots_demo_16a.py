import os,re,json
import AtlasStyle
from Drawer_ControlPlots import DrawAPI

import ROOT as R

R.gROOT.SetBatch(1)




def GetErrorsVariations():
        
    errors = [
        ['Single top',         ['stopWt_DR',           'RadHigh'],    ],
        ['Single top',         ['stopWt_DR',           'RadLow'],    ],        
        ['t#bar{t}',           ['ttbar',            'RadLow'],   ],
    ]

    #ttbar PDF
    for n in range(30):
        errors.append(['t#bar{t}',    ['ttbar',   'PDF4LHC_0'],   ['ttbar',  'PDF4LHC_%s'%(n+1)] ])
    
    #stop PDF
    for n in range(30):
        errors.append(['Single top',   ['stopWt_DR', 'PDF4LHC_0'],     ['stopWt_DR', 'PDF4LHC_%s'%(n+1)] ])

    return errors


#ttbar ME/Shower, stop DR/DS
def GetErrorsSamles():
    errors = [
        #  sample                      key1                 key2                    file
        ['t#bar{t}',           ['ttbar_aMcAtNloPy8',        None],      ],
        ['t#bar{t}',           ['ttbar_PowhegHerwig7',              None],    ],
        ['t#bar{t}',           ['ttbar_radHi',              None],    ],
        ['Single top',         ['stopWt_DS',                 None],              ],

    ]
    return errors

#style = 'JetF,EvtF,Sam'

class PlotInfoHandler(object):    
    def __init__(self,period):
        self.period = period


    #style = 'jetFlav,eventFlav,sample'
    def BookProcess(self,process):
        if process == 'sample':
            res = self.__BookProcess_sample()
        elif process == 'jetFlav':
            res = self.__BookProcess_jetFlav()
        elif process == 'eventFlav':
            res = self.__BookProcess_evetFlav()
        else:
            raise ValueError('Process "%s"not supported yet'%(process))

    def __BookProcess_sample(self):
        res = {
            'Data' : [
                ['Data','data'],                
            ],
            'MC' : [
                ['t#bar{t}',       R.kWhite,      'ttbar',          None], 
                ['Single top',     R.kRed,        'stopWt_DR',      None],
                ['Mis. leptons',   R.kGreen,      'fake',           None],
                ['z+jets',         R.kBlue,       'Ztt_sh221',      None],  
                ['Diboson',        R.kYellow,     'VV_Sh222',       None],
            ]
        }
        return res

    def __BookProcess_jetFlav(self):
        res = {
            'Data' : [
                ['Data','data'],                
            ],
            'MC' : [
                ['t#bar{t} b-jets',       R.kWhite,      'ttbar',          'b',], 
                ['L',     R.kRed,        'stopWt_DR',      None],
                ['other',   R.kGreen,      'fake',           None],
            ]
        }



def GetSamples(style): 
        samples = {
            'Data' : [
                ['Data','data'],                
            ],
        }
        if style == 'Sam':
            samples['MC'] = [
                ['t#bar{t}',       R.kWhite,      'ttbar',          None], 
                ['Single top',     R.kRed,        'stopWt_DR',      None],
                ['Mis. leptons',   R.kGreen,      'fake',           None],
                ['Mis. leptons',   R.kGreen,      'fake',           None],
                ['z+jets',         R.kBlue,       'Ztt_sh221',      None],  
                ['Diboson',        R.kYellow,     'VV_Sh222',       None],
            ]
        elif style == 'JetF':
            samples['MC'] = [
                ['']
            ]

        return samples


def GetTexts(category,variable):
    if 'PxT' in variable:
        text1 = 'Probe jets'
    elif 'CalJet' in variable:
        text1 = 'Pre tag jets'
    elif 'BDT' in variable:
        text1 = 'BDT score'
    elif variable in ['n_fjets', 'pt_jet0', 'pt_lep0', 'met', 'minl1j','pt_jet1', 'm_lj']:
        text1 = 'BDT input variables'
    elif variable in ['Mll','MET'] and '1ptag2jet' in category:
        text1 = '>= 1 tagged events'
    elif variable in ['Mll','MET'] and '0ptag2jet' in category:
        text1 = 'pre tag events'

    texts = [
        ['#font[72]{ATLAS}',0.2,0.844,1,0.05*1.58],
        ['#font[42]{Internal}',0.37, 0.844,1,0.05*1.58],
        ['#font[42]{#sqrt{s}=13 TeV, 36.1 fb^{-1}}',0.2, 0.77, 1, 0.0375*1.58],
        ['#font[42]{%s}'%(text1),0.2, 0.7, 1, 0.03*1.58],
    ]
    return texts


def GetPlot_info(variable):
    plot_info = {
        'xRange':None,#[0,600],
        'xBins': None,#[0,20,30,40,60,85,110,140,175,250,600],
        'yRange':None,        
    }
    if 'JetPt' in variable:
        info = {
            'unit':10.,
            'LogScale':True,
            'xTitle':'p_{T}(jet) [GeV]',
            'yTitle':'Jets / 10 GeV',
        }
        
    elif 'JetEta' in variable:
        info = {
            'xRange':[-3.,3.],
            'xBins':[-3+0.5*n for n in range(13)],
            'unit':0.5, 
            'xTitle':'#eta(jet)',
            'yTitle':'Jets / 0.5',
        }
        
    elif 'JetMV2c10' in variable:
        info = {
            'unit':0.1,
            'LogScale':True,
            'xTitle':'MV2c10',
            'yTitle':'Jets / 0.1',    
        }
    elif 'Mll' in variable:
        info = {
            'xRange':[0.,500.],
            'xBins':[20.*n for n in range(26)],
            'unit':20.,         
            'xTitle':'m_{ll} [GeV]',
            'yTitle':'Events / 20 GeV',
        }
    elif 'MET' in variable:
        info = {
            'xBins':[10.*n for n in range(31)],
            'xTitle':'E_{T}^{miss} [GeV]',
            'yTitle':'Entries / 10 GeV',
        }
    elif 'n_fjets' in variable:
        info = {
            'xBins':[n*1. for n in range(8)], 
            'xTitle' : 'Num of Fjets',
            'yTitle' : 'Entries',
        }
    elif 'pt_jet0' in variable:
        info = {
            'xBins':[10.*n for n in range(61)], 
            'xTitle':'p_{T}(jet0) [GeV]',
            'yTitle':'Entries / 10 GeV',            
        }        
    elif 'pt_lep0' in variable:
        info = {
            'xBins':[10.*n for n in range(31)], 
            'xTitle':'p_{T}(lep0) [GeV]',
            'yTitle':'Entries / 10 GeV',
        }
    elif 'met' in variable:
        info = {
            'xBins':[10.*n for n in range(31)], 
            'xTitle':'E_{T}^{miss} [GeV]',
            'yTitle':'Entries / 10 GeV',            
        }
    elif 'minl1j' in variable:
        info = {
            'xBins':[0.2*n for n in range(26)], 
            'xTitle':'Min#Delta R(l0,j)}',
            'yTitle':'Entries / 0.2',
        }

    elif 'pt_jet1' in variable:
        info = {
            'xBins':[10.*n for n in range(61)], 
            'xTitle':'p_{T}(jet0) [GeV]',
            'yTitle':'Entries / 10 GeV',
        }
    elif 'm_lj' in variable:
        info = {
            'xBins':[10.*n for n in range(31)], 
            'xTitle':'m_{lj}',
            'yTitle':'Entries / 10 GeV',
        }
    elif 'BDT' in variable :
        info = {
            'xRange':[-0.65,0.45],
            'xBins':[-0.65+n*0.05 for n in range(23)],
            'unit':0.1,        
            'xTitle' : 'D_{bb}^{T&P}',
            'yTitle' : 'Events / 0.1',
        }
    else:
        raise RuntimeError(category+variable)
    plot_info.update(info)
    return plot_info

def GetControlPlotsDemo(category,variable,flavComp=0): # flavComp = 0(no flav)/1(jet flav)/2(event flav)

    samples = GetSamples()
    texts = GetTexts(category,variable)

    plot_info = GetPlot_info(variable)

    


    dataFormat = '{0:}_%s_%s'%(category,variable)
    mcFormat   = '{0:}_%s_%s'%(category,variable)

    mcFormatSys = 'Sys{1:}/{0:}_%s_%s_Sys{1:}'%(category,variable)
    
    name = 'C_%s_%s'%(category,variable)

    

    errorsSyst = GetErrorsVariations()
    errorsTHEO = GetErrorsSamles()

    errors = {
        'modelling':[
            [errorsSyst,mcFormatSys],
            [errorsTHEO,mcFormat],
        ],        
    }

    DrawPlot(name, samples, plot_info, texts, outputPath, dataFormat, mcFormat,errors)



def DrawPlot(pic_name,samples,plot_info,texts,outputPath,dataFormat,mcFormat,errors):
    drawAPI = DrawAPI(outputPath)

    for entry in samples['Data']:
        drawAPI.AddData(*entry)

    for entry in samples['MC']:
        drawAPI.AddMCNominal(*entry)

    for name,items in errors.iteritems():
        drawAPI.BookErrorBand(name,items)

    drawAPI.SetPlot(pic_name, dataFormat, mcFormat,texts,plot_info)
    drawAPI.Draw()


def main():
    #ttbar_0ptag2jet_TP_MET
    #ttbar_0ptag2jet_TP_Mll
    #ttbar_1ptag2jet_TP_MVA80_MV2c10_FixedCut_Mll
    #ttbar_1ptag2jet_TP_MVA80_MV2c10_FixedCut_MET
    #probe jets info
    #pretag jets info
    #ttbar_0ptag2jet_TP_CalJetPt
    #bdt info
    #ttbar_1ptag2jet_TP_m_lj


    #probe jets info
    category = '1ptag2jet_TP_MVA80_MV2c10_FixedCut'
    variables = ['PxT85_CalJet%s'%(key) for key in ['Pt','Eta','MV2c10']]
    for variable in variables:
        GetControlPlotsDemo(category, variable)

    #pre tag jets info
    category = '0ptag2jet_TP'
    variables = ['CalJet%s'%(key) for key in ['Pt','Eta','MV2c10']]
    for variable in variables:
        GetControlPlotsDemo(category, variable)            

    #1ptag event info
    category = '1ptag2jet_TP_MVA80_MV2c10_FixedCut'
    variables = ['Mll','MET']
    for variable in variables:
        GetControlPlotsDemo(category, variable)
    
    #pretag event info
    category = '0ptag2jet_TP'
    variables = ['Mll','MET']
    for variable in variables:
        GetControlPlotsDemo(category, variable)

    #mva variables
    category = '1ptag2jet_TP'
    variables = ['n_fjets', 'pt_jet0', 'pt_lep0', 'met', 'minl1j','pt_jet1', 'm_lj','BDT']
    for variable in variables:
        GetControlPlotsDemo(category, variable)



if __name__ == '__main__':
    inputPath = '/home/ichen/CxAODFWs/FTAG_awesome/run/ReaderOutput/Reader_CxAOD_FTAG2_2L_mc16a_condor_1.1/hadd/samples'
    outputPath = './plots_mc16a'
    if not os.path.isdir(outputPath):
        os.system('mkdir %s'%(outputPath))
    main()
