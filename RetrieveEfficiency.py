import operator,math,re,toolkit,json,os
import ROOT as R

class Hist(object):

    nbins = 9

    def __init__(self,vals=None,errs=None):
        if vals != None:
            self.vals = vals
        else:
            self.vals = [0.]*Hist.nbins
        if errs != None:
            self.errs = errs            
        else:
            self.errs = [0.]*Hist.nbins

    def Unit(self):
        return Hist(vals=[1.]*Hist.nbins,errs=[0.]*Hist.nbins)

    def Scale(self,sf):
        self.vals = list(map(lambda x:x*sf, self.vals))
        self.errs = list(map(lambda x:x*sf, self.errs))

    def Sqrt(self):
        try:
            res = Hist(vals=map(math.sqrt,self.vals))
        except ValueError:
            print 'vals: ',self.vals
            raise ValueError
        return res

    def Abs(self):
        return Hist(vals=map(abs,self.vals))

    def Add(self,other):
        if not isinstance(other, Hist):            
            raise ValueError('Hist Add only works for Hist object')
        self.vals = list(map(operator.add,self.vals,other.vals))
        self.errs = list(map(lambda x,y:math.sqrt(x**2+y**2),self.errs,other.errs))

    def Operation(self,other,operator_val):
        if not isinstance(other, (Hist,float)):
            raise ValueError('Hist bi-operation only works for Hist/float object')        
        if isinstance(other, Hist):
            res = Hist(vals=map(operator_val,self.vals,other.vals))
        else:
            res = Hist(vals=map(lambda x:operator_val(x,other),self.vals))
        return res

    def __str__(self):
        return zip(self.vals,self.errs).__str__()
    def __repr__(self):
        return self.__str__()    
    def __add__(self,other):
            return self.Operation(other,operator.add)
    def __sub__(self,other):
        return self.Operation(other,operator.sub)
    def __mul__(self,other):
        return self.Operation(other,operator.mul)
    def __div__(self,other):
        def div(a,b):
            res = 0.            
            if b!=0:
                res = a/b
            return res
        return self.Operation(other, div)
    def __pow__(self,n):
        return Hist(vals=list(map(lambda x:x**n,self.vals)))

class StatErrorHandler(object):
    def __init__(self,seed):
        self.seed = seed
    def RollPoisson(self,tot,pas):
        seed = self.seed
        if tot == 0.:
            ratio = 0.
        else:
            ratio = float(pas)/float(tot)
        r_tot = int(round(seed.Poisson(tot)))
        r_pas = seed.Binomial(r_tot,ratio)
        return r_tot,r_pas

    def RollGauss(self,tot,pas,tot_err):
        seed = self.seed
        tot = 0. if tot<0. else tot
        pas = 0. if pas<0. else pas

        if tot==0.:
            ratio = 0.
        elif tot<pas:
            ratio = 1.
        else:
            ratio = pas/tot

        gaus = lambda x,y:abs(seed.Gaus(x,y))
        pas_err = tot_err*math.sqrt(ratio*(1-ratio))
        r_tot = abs(seed.Gaus(tot,tot_err))
        r_pas = abs(seed.Gaus(r_tot*ratio,pas_err))
        return r_tot,r_pas

    def RollHist(self,totHist,pasHist,model):
        tot_vals = totHist.vals
        tot_errs = totHist.errs
        pas_vals = pasHist.vals
        pas_errs = pasHist.errs

        r_tot_vals = []
        r_pas_vals = []
        for n in range(len(tot_vals)):
            tot = tot_vals[n]
            pas = pas_vals[n]
            tot_err = tot_errs[n]
            pas_err = pas_errs[n]
            if model == 'Poisson':
                r_tot,r_pas = self.RollPoisson(tot, pas)
            elif model == 'Gaus':
                r_tot,r_pas = self.RollGauss(tot, pas,tot_err)
            else:
                raise ValueError('model should be Poisson or Gaus only')
            r_tot_vals.append(r_tot)
            r_pas_vals.append(r_pas)
        r_totHist = Hist(vals=r_tot_vals)
        r_pasHist = Hist(vals=r_pas_vals)
        return r_totHist,r_pasHist


class RetrieveRawHist(toolkit.IOHandler):
    def __init__(self,inputFile,logDir):
        self.inputFile = R.TFile(inputFile,'read')        
        
        self.data = {}

        logFile = '{0:}/log_RetrieveRawHist.%s'.format(logDir)
        if not os.path.isdir(logDir):
            os.system('mkdir -p %s'%(logDir))
        
        self.BindOutput('Info', 'Info', logFile%('out'))
        self.BindOutput('Warning', 'Warning', logFile%('warning'))
        self.BindOutput('FBIWarning', 'FBIWarning', logFile%('fbiwarning'))




    def retrieveSample(self,sample,slot,histNames,format):
        '''
        |--
            tt 
                |-- PowHegPythia8
                    |-- PxT
                    |-- PxP
                    |-- PbT
                    |-- PbP
                    |-- PjT
                    |-- PjP
                |-- PowHegPythia8_JES_1up
                    ...
                |-- aMC@NLO
                    ...
            data
                |-- PxT
                |-- PxP
        '''
        
        if not sample in self.data:
            self.data[sample] = {}
        if not slot in self.data[sample]:
            self.data[sample][slot] = {}
        if sample in ['data','fake']:
            tpKeys = ['PxP','PxT']
        else:
            tpKeys = ['PxP','PxT','PbP','PbT','PjP','PjT']        

        entry = self.retrieveHist(histNames, tpKeys, format)
        self.data[sample][slot] = entry


    def retrieveHist(self,histNames,tpKeys,format):
        entry = {}        
        for tp in tpKeys:
            hist = Hist()
            for name in histNames:            
                hname = format.format(sample=name,tp=tp)
                th1 = self.inputFile.Get(hname)
                if th1==None:
                    self.Warning('TH1 not exit in root file',hname,self.inputFile)
                else:
                    vals,errs = self.LoadHist(th1)
                    temp_hist = Hist(vals=vals,errs=errs)
                    hist.Add(temp_hist)
            entry[tp] = {'vals':hist.vals,'errs':hist.errs}

        return entry



    def LoadHist(self,th1):
        vals = []
        errs = []
        for n in range(2,Hist.nbins+2):
            vals.append(th1.GetBinContent(n))
            errs.append(th1.GetBinError(n))
        return vals,errs
    


    def dumpRawHist(self,outputJson):
        data = self.data
        res = json.dumps(data,indent=4,sort_keys=True)
        with open(outputJson,'w') as f:
            f.write(res)

class Caliber(object):
    def __init__(self,inputFile):
        pass

class RetrieveEfficiency(toolkit.IOHandler):
    '''
    stages for entries:
        raw_entry -> fetched from a dedicated sample composition
        medium_entry ->  tt/nt/data
        well_entry -> sf./eff...        
    '''
    def __init__(self,inputRawJson,logPath):
        self.debug = False
        self.data = json.load(open(inputRawJson,'r'))

        if not os.path.isdir(logPath):
            os.system('mkdir -p %s'%(logPath))        
        logFile = '{0:}/log_RetrieveEfficiency.%s'.format(logPath)

        self.BindOutput('Info', 'Info', logFile%('out'))
        self.BindOutput('Warning', 'Warning', logFile%('warning'))
        self.BindOutput('FBIWarning', 'FBIWarning', logFile%('fbiwarning'))
        

    def getRawEntry(self,slot_map):
        raw_entry = {}
        for sample,slot in slot_map.iteritems():
            raw_entry[sample] = {}
            if isinstance(slot, dict):
                raw_entry[sample] = self.getPolyRawEntry(slot)
            else:
                try:
                    data = self.data[sample][slot]            
                except TypeError:
                    print sample,slot
                for tp,content in data.iteritems():
                    hist = Hist(**content)
                    raw_entry[sample][tp] = hist
        return raw_entry

    def getPolyRawEntry(self,slot_map):
        poly_raw_entry = {}
        for sample,slot_name in slot_map.iteritems():            
            for tpKey,content in self.data[sample][slot_name].iteritems():
                if not tpKey in poly_raw_entry:
                    poly_raw_entry[tpKey] = Hist()
                hist = Hist(**self.data[sample][slot_name][tpKey])
                
                poly_raw_entry[tpKey].Add(hist)
                #print 'poly',sample,slot_name,tpKey,self.data[sample][slot_name][tpKey]['vals']
        return poly_raw_entry

    #get tt/nt/data
    def getMediumEntry(self,raw_entry):
        medium_entry = {}
        medium_entry['data'] = raw_entry['data']
        medium_entry['tt'] = raw_entry['tt']
        medium_entry['nt'] = {}

        #add fake+MCs
        for tp in ['PxP','PxT']:
            hist = Hist()
            for sample in ['stop','zjets','diboson','fake']:
                temp = raw_entry[sample][tp]
                hist.Add(temp)
            medium_entry['nt'][tp] = hist

        #add MCs
        for tp in ['PbP','PbT','PjP','PjT']:
            hist = Hist()
            for sample in ['stop','zjets','diboson']:
                temp = raw_entry[sample][tp]
                hist.Add(temp)
            medium_entry['nt'][tp] = hist                
        return medium_entry

    def CalculateEfficiencies(self,medium_entry):
        

        dt = medium_entry['data']
        tt = medium_entry['tt']        
        nt = medium_entry['nt']

        if self.debug:    
            data = {'dt':dt,'tt':tt,'nt':nt}
            for sample,entry in data.iteritems():
                for key,value in entry.iteritems():
                    print sample,key,'{0:.2f}'.format(value.vals[0])
            print 
            for key in ['PxP','PxT']:
                _dt = data['dt'][key].vals[0]
                _tt = data['tt'][key].vals[0]
                _nt = data['nt'][key].vals[0]

                print 'data {0:.2f}  tt {1:.2f}  nt {2:.2f}   tt+nt {3:.2f}'.format(_dt,_tt,_nt,_tt+_nt)
    
        well_entry = {}
        I = Hist().Unit()
        
        well_entry['f_tb'] = tt['PbT']/(tt['PbT']+tt['PjT'])
        well_entry['e_nb'] = tt['PjP']/tt['PjT']
        well_entry['e_mc'] = tt['PbP']/tt['PbT'] 
        well_entry['e_tt'] = (dt['PxP']-nt['PxP'])/(dt['PxT']-nt['PxT'])
        well_entry['e_dt'] = (well_entry['e_tt']-well_entry['e_nb']*(I-well_entry['f_tb']))/well_entry['f_tb']
        well_entry['sf'] = well_entry['e_dt']/well_entry['e_mc']
        
        return well_entry

    def CalculateDiffs(self,up,down):
        res = {}
        for key in up.keys():
            hist = ((up[key] - down[key])/2.).Abs()
            res[key] = hist
        return res

    def CalculateStat(self,medium_entry):
        nToys = 10000
        seed = R.TRandom3()
        statHandler = StatErrorHandler(seed)

        well_entry = self.CalculateEfficiencies(medium_entry)

        emptyHist = lambda x:Hist()
        addM1 = lambda x,y:x+y/float(nToys)
        addM2 = lambda x,y:x+(y**2)/float(nToys)

        m1_dataStat = self.mapEntry(emptyHist,well_entry)
        m2_dataStat = self.mapEntry(emptyHist,well_entry)
        m1_mcStat = self.mapEntry(emptyHist,well_entry)
        m2_mcStat = self.mapEntry(emptyHist,well_entry)

        for _ in range(nToys):
            r_entry_dataStat,r_entry_mcStat = self.Roll(medium_entry,statHandler)

            m1_dataStat = self.mapEntry(addM1, m1_dataStat, r_entry_dataStat)
            m1_mcStat = self.mapEntry(addM1, m1_mcStat, r_entry_mcStat)

            m2_dataStat = self.mapEntry(addM2,m2_dataStat, r_entry_dataStat)
            m2_mcStat = self.mapEntry(addM2,m2_mcStat, r_entry_mcStat)

        m1_dataStat_square = self.mapEntry(lambda x:x**2, m1_dataStat)
        m1_mcStat_square = self.mapEntry(lambda x:x**2, m1_mcStat)        
        
        dataStat = {}

        for key in ['sf','e_tt','e_dt']:
            dataStat[key] = (m2_dataStat[key]-m1_dataStat_square[key]).Sqrt()
        for key in ['e_mc','e_nb','f_tb']:
            dataStat[key] = Hist()
        mcStat = {}
        for key in ['sf','e_tt','e_dt','e_mc','e_nb','f_tb']:
            mcStat[key] = (m2_mcStat[key]-m1_mcStat_square[key]).Sqrt()
        return dataStat,mcStat

    # for well entry            
    def mapEntry(self,transformer,entryA,entryB=None):
        res = {}
        for key in entryA.keys():                    
                if entryB == None:
                    res[key] = transformer(entryA[key])
                else:
                    res[key] = transformer(entryA[key],entryB[key])
        return res

    def Roll(self,medium_entry,statHandler):
        
        dt = medium_entry['data']
        tt = medium_entry['tt']
        nt = medium_entry['nt']

        r_dt = {}
        r_tt = {}
        r_nt = {}
        
        r_dt['PxT'],r_dt['PxP'] = statHandler.RollHist(dt['PxT'],dt['PxP'],'Poisson')
        r_tt['PxT'],r_tt['PxP'] = statHandler.RollHist(tt['PxT'],tt['PxP'],'Gaus')
        r_tt['PjT'],r_tt['PjP'] = statHandler.RollHist(tt['PxT'],tt['PxP'],'Gaus')
        r_tt['PbT'],r_tt['PbP'] = statHandler.RollHist(tt['PxT'],tt['PxP'],'Gaus')
        r_nt['PxT'],r_nt['PxP'] = statHandler.RollHist(nt['PxT'],nt['PxP'],'Gaus')
        r_nt['PjT'],r_nt['PjP'] = statHandler.RollHist(nt['PxT'],nt['PxP'],'Gaus')
        r_nt['PbT'],r_nt['PbP'] = statHandler.RollHist(nt['PxT'],nt['PxP'],'Gaus')    
        
        
        r_entry_mcStat = {}        
        r_entry_mcStat['data'] = dt # invariant
        r_entry_mcStat['tt'] = r_tt
        r_entry_mcStat['nt'] = r_nt
        
        r_entry_dataStat = {}        
        r_entry_dataStat['tt'] = tt # invariant
        r_entry_dataStat['nt'] = nt # invariant
        r_entry_dataStat['data'] = r_dt


        r_dataStat = self.CalculateEfficiencies(r_entry_dataStat)
        r_mcStat = self.CalculateEfficiencies(r_entry_mcStat)

        return r_dataStat,r_mcStat

    def dump_result(self,outputJson):
        result = {}
        for key in ['sf','e_tt','e_dt','e_mc','e_nb','f_tb']:
            result[key] = {}
            for syst,well_entry in self.result.iteritems():
                result[key][syst] = well_entry[key].vals
        outputStr = json.dumps(result,indent=4,sort_keys=True)

        with open(outputJson,'w') as f:
            f.write(outputStr)
        
    def GetDiffError(self,slot_map_up,slot_map_down=None):

        slot_up = {key:self.slot_map_nominal[key] for key in self.slot_map_nominal}
        slot_up.update(slot_map_up)
        well_entry_up = self.getWellEntry(slot_up)

        slot_down = {key:self.slot_map_nominal[key] for key in self.slot_map_nominal}
        if slot_map_down != None:
            slot_down.update(slot_map_down)
        well_entry_down = self.getWellEntry(slot_down)
                
        res = self.differCalcu(well_entry_up, well_entry_down)

        return res

    def differCalcu(self,well_entry_up,well_entry_down):
        transformer = lambda hup,hdown:((hup-hdown)/2.).Abs()
        res = self.mapEntry(transformer, well_entry_down,well_entry_up)
        return res        

    def GetNormalization(self,up_scale,down_scale):
        raw_entry_up = self.getRawEntry(self.slot_map_nominal)
        well_entry_up = self.normalizer(raw_entry_up,up_scale)

        raw_entry_down = self.getRawEntry(self.slot_map_nominal)
        well_entry_down = self.normalizer(raw_entry_down,down_scale)

        res = self.differCalcu(well_entry_up, well_entry_down)
        return res

    def normalizer(self,raw_entry,scaler):
        for sample,content in raw_entry.iteritems():
            factor = scaler.get(sample)
            if not factor:
                continue
            else:
                for tp,hist in content.iteritems():
                    hist.Scale(factor)
        medium = self.getMediumEntry(raw_entry)
        well = self.CalculateEfficiencies(medium)
        return well

    def getWellEntry(self,slot_map):
        raw_entry = self.getRawEntry(slot_map)
        medium_entry = self.getMediumEntry(raw_entry)
        well_entry = self.CalculateEfficiencies(medium_entry)
        return well_entry

def main():
    pass

if __name__ == '__main__':
    main()