import os
import ROOT as R
import AtlasStyle
from Drawer_ControlPlots import DrawAPI
version = 'default'
def getLumi(period):
    lumiMap = {
        '1516':36.1,
        '17':43.8,        
        '18':59.9,
    }
    lumi = 0.
    for key,value in lumiMap.iteritems():
        if key in period:
            lumi += value
    return lumi


class Configuration(object):

    def process_probe_jets_flav(self,nominalFmt):
        res = {
            'Data' : [
                ['Data',nominalFmt,'data'],                
            ],
            'MC' : [
                ['t#bar{t} b-jets',    nominalFmt,  'ttbar',      '_b',], 
                ['t#bar{t} non-bjets', nominalFmt, 'ttbar',      ['_c','_l','_x']],
                ['non-t#bar{t}',       nominalFmt,  ['VV_Sh222','Ztt_sh221','stopWt_DR'],       ['_b','_c','_l','_x']],
                ['non-t#bar{t}',       nominalFmt,       'fake'],
            ]
        }
        return res

    def process_sample_combination(self,nominalFmt):
        res = {
            'Data' : [
                ['Data',nominalFmt,'data'],                
            ],
            'MC' : [
                ['t#bar{t}',      nominalFmt, 'ttbar',      ], 
                ['Single top',    nominalFmt, 'stopWt_DR',  ],
                ['z+jets',        nominalFmt, 'Ztt_sh221',  ],  
                ['Diboson',       nominalFmt, 'VV_Sh222',   ],
                ['Mis. leptons',  nominalFmt, 'fake',       ],                
            ]
        }
        return res    

    def process_event_flav(self,nominalFmt):
        samples = ['ttbar','Ztt_sh221','stopWt_DR','VV_Sh222']        
        res = {
            'Data' : [
                ['Data',nominalFmt,'data'],                
            ],
            'MC' : [
                ['(b,b)',  nominalFmt,    samples,      '_bb',], 
                ['(b,l)',  nominalFmt,    samples,      '_bl'],
                ['other',  nominalFmt,    samples,      ['_bc','_bx','_cc','_cl','_cx','_ll','_lx']],
                ['other',  nominalFmt,    'fake',      ],
            ]
        }
        return res

    def colorScheme_probe_jets_flav(self):
        res = [
            # ['t#bar{t} b-jets',R.kWhite],
            # ['t#bar{t} non-bjets',R.kRed],
            # ['non-t#bar{t}',R.kBlue],
            ['t#bar{t} b-jets',0],
            ['t#bar{t} non-bjets',16],
            ['non-t#bar{t}', 13],            
        ]
        return res

    def colorScheme_sample_combination(self):
        res = [
            ['t#bar{t}',       R.kWhite, ], 
            ['Single top',     R.kRed,   ],
            ['z+jets',         R.kBlue,  ],  
            ['Diboson',        R.kYellow,],
            ['Mis. leptons',   R.kGreen, ],
        ]
        return res

    def colorScheme_event_flav(self):
        res = [
            ['(b,b)', R.kWhite],
            ['(b,l)', R.kRed],
            ['other', R.kBlue],
        ]
        return res

    def errorBand_Full(self,nominalFmt,systFmt):    
        res = {
            'tt HardScatter' : [ 
                [nominalFmt,'ttbar'], 
                [nominalFmt,'ttbar_aMcAtNloPy8']
            ],
            'tt Fragmentation' : [
                [nominalFmt,'ttbar'],
                [nominalFmt,'ttbar_PowhegHerwig7']
            ],            
            'tt Radiation High' : [
                [nominalFmt,'ttbar'], 
                [nominalFmt,'ttbar_radHi']
            ],
            'tt Radiation Low' : [
                [nominalFmt,'ttbar'], 
                [systFmt,'ttbar','RadLow']
            ], 
            'stop DRDS' : [
                [nominalFmt,'stopWt_DR'],
                [nominalFmt,'stopWt_DS']
            ],
            'stop Radiation High' : [
                [nominalFmt,'stopWt_DR'], 
                [systFmt,'stopWt_DR','RadHigh']
            ],
            'stop Radiation Low' : [
                [nominalFmt,'stopWt_DR'], 
                [systFmt,'stopWt_DR','RadLow']
            ],
            'zjets Modelling' : [
                [nominalFmt,'Ztt_sh221'],
                [nominalFmt,'Ztt_Mad']
            ],
            'diboson Modelling' : [
                [nominalFmt,'VV_Sh222'],
                [nominalFmt,'VV_PwPy8']
            ],
        }

        for n in range(1,31):
            res['tt PDF Error %s'%(n)] = [ 
                [systFmt,'ttbar','PDF4LHC_0'], 
                [systFmt,'ttbar','PDF4LHC_%s'%(n)]
            ]
            res['stop PDF Error %s'%(n)] = [
                [systFmt,'stopWt_DR','PDF4LHC_0'], 
                [systFmt,'stopWt_DR','PDF4LHC_%s'%(n)]
            ]

        return res



    def texts(self,period,region):
        regions = {
            'Probes':'Probe jets',
        }

        res = [
            ['#font[72]{ATLAS}',0.2,0.844,1,0.05*1.58],
            ['#font[42]{Internal}',0.37, 0.844,1,0.05*1.58],
            ['#font[42]{#sqrt{s}=13 TeV, %s fb^{-1}}'%(getLumi(period)),0.2, 0.77, 1, 0.0375*1.58],
            ['#font[42]{%s}'%(regions[region]),0.2, 0.7, 1, 0.03*1.58],
        ]
        return res

    def plot_info(self,variable):
        plot_info = {
            'xRange':None,
            'xBins': None,
            'yRange':None,        
        }
        if 'JetPt' in variable:
            info = {
                'unit':10.,
                'LogScale':True,
                'xTitle':'p_{T}(jet) [GeV]',
                'yTitle':'Jets / 10 GeV',
            }
            
        elif 'JetEta' in variable:
            info = {
                'xRange':[-3.,3.],
                'xBins':[-3+0.5*n for n in range(13)],
                'unit':0.5, 
                'xTitle':'#eta(jet)',
                'yTitle':'Jets / 0.5',
            }
            
        elif 'JetMV2c10' in variable:
            info = {
                'unit':0.1,
                'LogScale':True,
                'xTitle':'MV2c10',
                'yTitle':'Jets / 0.1',    
            }
        elif 'Mll' in variable:
            info = {
                'xRange':[0.,500.],
                'xBins':[20.*n for n in range(26)],
                'unit':20.,         
                'xTitle':'m_{ll} [GeV]',
                'yTitle':'Events / 20 GeV',
            }
        elif 'MET' in variable:
            info = {
                'xBins':[10.*n for n in range(31)],
                'xTitle':'E_{T}^{miss} [GeV]',
                'yTitle':'Entries / 10 GeV',
            }
        elif 'n_fjets' in variable:
            info = {
                'xBins':[n*1. for n in range(8)], 
                'xTitle' : 'Num of Fjets',
                'yTitle' : 'Entries',
            }
        elif 'pt_jet0' in variable:
            info = {
                'xBins':[10.*n for n in range(61)], 
                'xTitle':'p_{T}(jet0) [GeV]',
                'yTitle':'Entries / 10 GeV',            
            }        
        elif 'pt_lep0' in variable:
            info = {
                'xBins':[10.*n for n in range(31)], 
                'xTitle':'p_{T}(lep0) [GeV]',
                'yTitle':'Entries / 10 GeV',
            }
        elif 'met' in variable:
            info = {
                'xBins':[10.*n for n in range(31)], 
                'xTitle':'E_{T}^{miss} [GeV]',
                'yTitle':'Entries / 10 GeV',            
            }
        elif 'minl1j' in variable:
            info = {
                'xBins':[0.2*n for n in range(26)], 
                'xTitle':'Min#Delta R(l0,j)}',
                'yTitle':'Entries / 0.2',
            }

        elif 'pt_jet1' in variable:
            info = {
                'xBins':[10.*n for n in range(61)], 
                'xTitle':'p_{T}(jet0) [GeV]',
                'yTitle':'Entries / 10 GeV',
            }
        elif 'm_lj' in variable:
            info = {
                'xBins':[10.*n for n in range(31)], 
                'xTitle':'m_{lj}',
                'yTitle':'Entries / 10 GeV',
            }
        elif 'BDT' in variable :
            info = {
                'xRange':[-0.65,0.45],
                'xBins':[-0.65+n*0.05 for n in range(23)],
                'unit':0.1,        
                'xTitle' : 'D_{bb}^{T&P}',
                'yTitle' : 'Events / 0.1',
            }
        else:
            raise RuntimeError(category+variable)

        plot_info.update(info)
        return plot_info


class Draw(object):
    def go(self,period,inputFile,outputPath):

        #probe jets properties
        region = 'Probes'
        for variable in ['CalJetPt']:#,'CalJetEta','CalJetMV2c10']:
            
            error_name = 'MC stat.+Modelling'
            nominalFmt = '{0:}_1ptag2jet{1:}_TP_MVA80_MV2c10_FixedCut_PxT85_%s'%(variable)
            systFmt    = 'Sys{1:}/{0:}_1ptag2jet_TP_MVA80_MV2c10_FixedCut_PxT85_%s_Sys{1:}'%(variable)            
            plot_info = Configuration().plot_info(variable)
            texts = Configuration().texts(period, region)
            errorScheme = Configuration().errorBand_Full(nominalFmt, systFmt)
            
            #process combination
            plot_name = 'ProbeJets_MVA80_%s_SampleComb'%(variable)            
            process = Configuration().process_sample_combination(nominalFmt)
            colorScheme = Configuration().colorScheme_sample_combination()                    
            DrawAPI(inputFile,outputPath).DrawPlot(plot_name,error_name,plot_info,texts,process,colorScheme,errorScheme)

            #probe-flav combination
            plot_name = 'ProbeJets_MVA80_%s_FlavComb'%(variable)            
            process = Configuration().process_probe_jets_flav(nominalFmt)
            colorScheme = Configuration().colorScheme_probe_jets_flav()
            plotting_style = {'hist-ratioBand':{'FillColor':16}}
            DrawAPI(inputFile,outputPath,plotting_style).DrawPlot(plot_name,error_name,plot_info,texts,process,colorScheme,errorScheme)            


def DrawControlPlots(period,inputFile):
    # period = 'Data1516'
    # inputFile = '/home/ichen/CxAODFWs/FTAG_awesome/run/ReaderOutput/Reader_CxAOD_FTAG2_2L_mc16a_condor_1.1/hadd/input_withSherpa.root'
    # inputFile = '/Users/cheng/workspace/root_data/input_mc16a_withSherpa.root'
    outputPath = './output.{0:}/ControlPlots_{1:}'.format(version,period)
    if not os.path.isdir(outputPath):
        os.system('mkdir -p %s'%(outputPath))
    Draw().go(period,inputFile,outputPath)



if __name__ == '__main__':
    main()