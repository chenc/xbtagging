import itertools
import ROOT as R 
import copy,math
from array import array
R.gROOT.SetBatch(1)
default_plotting_style = {
    'hist-data' : {
        'MarkerSize' : 0.8,
        'Xaxis' : {
            'LabelOffset':999,
        }
    },
    'stack-mc' : {
        'Xaxis' : {
            'LabelOffset':999,
            'LabelSize' : 0.,
        },
        'Yaxis':{                
            'TitleSize':0.06,
            'TitleOffset':1.35,
            'TitleFont':42,
            'LabelSize':0.06,
            'LabelFont':42,
            'LabelOffset':0.005,
            'Ndivisions':505,
        },            
    },
    'hist-ratio' : {
        'Xaxis':{                
            'TitleSize':0.13,
            'TitleOffset':1.2,
            'TitleFont':42,
            'LabelSize':0.11,
            'LabelFont':42,
            'LabelOffset':0.005,
            'Ndivisions':510,
        },
        'Yaxis':{
            'Title':'Data/Pred.',
            'TitleSize':0.11,
            'TitleOffset':0.74,
            'TitleFont':42,
            'LabelSize':0.08,
            'LabelFont':42,
            'LabelOffset':0.005,
            'Ndivisions':50104,
            'RangeUser':(0.2,1.8),
        },
    },
    'hist-ratioBand' : {
        'MarkerSize':0,
        'MarkerColor':3,
        'LineWidth':1,
        'FillStyle':3002,
        'FillColor':3,          
    },
    'pad-up' : {
        'constructor':('upPad','upPad',0,0.355,1,1),
        'settings'   :{
            'TopMargin':0.06,
            'BottomMargin':0.023,
            'LeftMargin':0.15,
            'RightMargin':0.045,
        },                
    },
    'pad-down' : {
        'constructor':('downPad','downPad',0,0,1,0.348),
        'settings'   :{
            'TopMargin':0.03,
            'BottomMargin':0.40,
            'LeftMargin':0.15,
            'RightMargin':0.045,
        },
    },
    'legend-up' : {
        'constructor':(0.65,0.6,0.9,0.9),  
        'settings':{
            'NColumns':1,
            'FillStyle':0,
            'BorderSize':0,
            'TextFont':62,
        },        
    },
    'legend-down' : {
            'constructor':(0.35,0.8,0.7,0.95),
            'settings':{
                'TextFont':62,
            },
    }
}



class DrawAPI(object):
    def __init__(self,file,output_path,plotting_style=None):
        self.output_path = output_path
        self.file = file
        
        self.UpdatePlottingStyle(plotting_style)

    def UpdatePlottingStyle(self,plotting_style=None):
        def update(A,B):
            for k,v in B.iteritems():
                if not k in A:
                    A[k] = v
                else:
                    if isinstance(v,dict) and isinstance(A[k],dict):
                        update(A[k], v)
                    else:
                        A[k] = v
        self.plotting_style = copy.deepcopy(default_plotting_style)                        
        if plotting_style != None:
            update(self.plotting_style, plotting_style)


    def DrawPlot(self,plot_name,error_name,plot_info,texts,process,colorScheme,errorScheme):
        self.name = plot_name
        
        self.data_entries = self.fill_process(process['Data'])
        self.mc_entries = self.fill_process(process['MC'])
        self.colorScheme = colorScheme

        self.error_name = error_name
        self.errorBandItems = errorScheme

        self.texts = texts
        self.plot_info = plot_info
        
        self.Draw()

    def fill_process(self,process):
        res = {}
        for entry in process:
            name,fmt,keys = entry[0],entry[1],entry[2:]            
            if not name in res:
                res[name] = []
            res[name].append([fmt,keys])        
        return res

    def Draw(self):
        histograms_handler = HistogramsHandler(self.file)
        hist_data = histograms_handler.GetData(self.data_entries)
        hist_mc = histograms_handler.GetMC(self.mc_entries)
                
        errorBandHist,errorBandDict = histograms_handler.GetErrorBand(self.errorBandItems,hist_mc)
        for k,v in errorBandDict.iteritems():
            pass
            # print k,list(map('{0:<6.2f}'.format,v))


        objects_handler = ObjectsHandler(self.colorScheme,self.plot_info,self.plotting_style)
        objects = objects_handler.GetObjects(hist_data,hist_mc,errorBandHist,self.error_name)

        pic_name = '{0:}/{1:}.png'.format(self.output_path,self.name)
        painter = Painter(pic_name)
        painter.PrintPlot(objects,self.plotting_style['stack-mc'],self.texts)

    def FileHandler(self,name,*file_sequence):
        for f in file_sequence:
            if f != None:
                return f
        raise ValueError('No file for {0:} provided'.format(name))



class TFileHandler(object):
    file_map = {}
    def __init__(self):
        pass
    def __call__(self,fileName):

        if not fileName in type(self).file_map:        
            type(self).file_map[fileName] = R.TFile(fileName,'read')

        return type(self).file_map[fileName]
        

class HistogramsHandler(object):
    def __init__(self,file=None):
        if file != None:
            self.file = file



    def GetData(self,entries):
        sum_hist = None        
        for name,sub_entries in entries.iteritems():
            for entry in sub_entries:
                hist = self.GetHist(*entry)
                if hist:
                    if sum_hist == None:
                        sum_hist = hist
                    else:
                        sum_hist.Add(hist)
        if not sum_hist:
            print 'file: %s'%(self.file)                            
            raise RuntimeError('no data hist found!')
        sum_hist.Scale(2.) # temporary fix
        return sum_hist

    def GetMC(self,mc_entries):
        mc_hists = {}
        for name,entries in mc_entries.iteritems():
            for entry in entries:                
                hist = self.GetHist(*entry)
                if hist:
                    if not name in mc_hists:
                        mc_hists[name] = hist
                    else:
                        mc_hists[name].Add(hist)
        return mc_hists

    def GetHist(self,fmt,keys):

        keys_wrapped = []
        for key in keys:
            if isinstance(key, str):
                key = [key]
            keys_wrapped.append(key)
        keys_iter = itertools.product(*keys_wrapped)

        sum_hist = None
        #print 'start --- '
        for key in keys_iter:
            hist_name = fmt.format(*(list(key)+['']*2))
            #print hist_name
            hist = TFileHandler()(self.file).Get(hist_name)
            if not hist:
                print 'skip empty hist {0:}'.format(hist_name)
                continue
            if sum_hist == None:
                sum_hist = hist.Clone()
            else:
                sum_hist.Add(hist)
        #print 'end --- '                
        return sum_hist

    def GetErrorBand(self,errorBandItems,mc_hists):

        sumHist = self.SumHist(mc_hists)
        central,mc_stat = self.GetHistContent(sumHist)
        
        errors = {}
        errors['mc stat'] = mc_stat
        sumError = [v for v in mc_stat]
        for name,item in errorBandItems.iteritems():
            entry_up = item[0]
            entry_down = item[1]
            errors[name] = self.GetErrorEntry(entry_up, entry_down)
            for n,v in enumerate(sumError):
                sumError[n] = math.sqrt(sumError[n]**2+errors[name][n]**2)
        for name,value in errors.iteritems():
            for n,v in enumerate(value):
                s = 0. if central[n] == 0. else v/central[n]
                value[n] = s

        self.SetHistContent(sumHist, central, sumError)

             
        return sumHist,errors

    def GetErrorEntry(self,up,down):        
        diff = lambda u,d:abs(u*0.5-d*0.5)
        up_vals,up_errs = self.GetHistContent(self.GetHist(up[0],up[1:]))
        down_vals,down_errs = self.GetHistContent(self.GetHist(down[0],down[1:]))
        error = list(map(diff,up_vals,down_vals))
        return error

    def SumHist(self,hists):
        sumHist = None
        for name,hist in hists.iteritems():
            if sumHist == None:
                sumHist = hist.Clone()
            else:
                sumHist.Add(hist)
        return sumHist

    def GetHistContent(self,hist):
        N = hist.GetNbinsX()
        centrals = []
        errors = []
        for n in range(N):
            c = hist.GetBinContent(n+1)
            e = hist.GetBinError(n+1)
            centrals.append(c)
            errors.append(e)
        return centrals,errors

    def SetHistContent(self,hist,central,error):
        N = hist.GetNbinsX()
        for n in range(N):
            c = central[n]
            e = error[n]
            hist.SetBinContent(n+1,c)
            hist.SetBinError(n+1,e)

            




class ObjectsHandler(object):
    def __init__(self,colorScheme=None,plot_info=None,plotting_style=None):
        
        self.colorScheme = colorScheme
        self.plot_info = plot_info        
        self.plotting_style = plotting_style


    def GetObjects(self,dataHist,mcHist,errorBand,error_name):

        _dataHist,_mcHist,_errorBand = self.ProcessHists(dataHist,mcHist,errorBand)

        canvas = self.GetCanvas()
        pad_up = self.GetPadUp()
        pad_down = self.GetPadDown()

        hstack_mc = self.GetTHStack(_mcHist)
        hist_data = self.GetHistData(_dataHist)

        hist_ratio = self.GetRatio(_dataHist,_mcHist)
        hist_ratio_band = self.GetRatioBand(_errorBand)

        legend_up = self.GetLegendUp(hist_data,_mcHist)
        legend_down = self.GetLegendDown(hist_ratio_band,error_name)

        objects = {
            'canvas' : canvas,
            'pad_up' : pad_up,
            'pad_down' : pad_down,
            'hstack_mc' : hstack_mc,
            'hist_data' : hist_data,
            'hist_ratio' : hist_ratio,
            'hist_ratio_band' : hist_ratio_band,
            'legend_up' : legend_up,
            'legend_down' : legend_down,
        }
        return objects

    def GetRatioBand(self,errBand):
        
        centrals,errors = HistogramsHandler().GetHistContent(errBand)
        errorRatio = []
        for n in range(len(centrals)):
            c = centrals[n]
            e = errors[n]
            if c == 0.:
                errorRatio.append(0.)
            else:
                r = e/c
                errorRatio.append(r)
        centrals_ratioBand = [1.] * len(centrals)
        ratioBand = errBand.Clone()

        HistogramsHandler().SetHistContent(ratioBand, centrals_ratioBand, errorRatio)

        self.DecorateObject(ratioBand, self.plotting_style['hist-ratioBand'])
        return ratioBand

    def GetLegendUp(self,dataHist,mcHist):
        
        legend = R.TLegend(*self.plotting_style['legend-up']['constructor'])
        self.DecorateObject(legend, self.plotting_style['legend-up']['settings'])

        legend.AddEntry(dataHist,'Data','p')

        for name,color in self.colorScheme:
            legend.AddEntry(mcHist[name],name,'f')
        return legend

    def GetLegendDown(self,ratioBand,error_name):
        legend = R.TLegend(*self.plotting_style['legend-down']['constructor'])
        self.DecorateObject(legend, self.plotting_style['legend-down']['settings'])        
        legend.AddEntry(ratioBand,error_name,'f')
        return legend

    def GetHistData(self,hist):
        dataHist = hist.Clone()
        self.DecorateObject(dataHist, self.plotting_style['hist-data'])        
        return dataHist


    def GetRatio(self,dataHist,mcHist):
        sumHist = HistogramsHandler().SumHist(mcHist)
        ratio = sumHist.Clone()
        ratio.Reset()
        ratio.Divide(dataHist,sumHist)

        self.DecorateObject(ratio, self.plotting_style['hist-ratio'])
        ratio.GetXaxis().SetTitle(self.plot_info['xTitle'])
        return ratio


    def ProcessHists(self,dataHist,mcHist,errorBand):

        _dataHist = dataHist.Clone()
        _mcHist = {name:hist.Clone() for name,hist in mcHist.iteritems()}
        _errorBand = errorBand.Clone()


        # re-binning
        if 'xBins' in self.plot_info and self.plot_info['xBins'] != None:
            xBins = self.plot_info['xBins']
            FooRebinner = lambda hist:hist.Rebin(len(xBins)-1,'',array('d', xBins)).Copy(hist)
            hists = [_dataHist] + _mcHist.values() + [_errorBand]
            for hist in hists:
                FooRebinner(hist)

        # re-unit  

        if 'unit' in self.plot_info:
            unit = self.plot_info['unit']              
        else:
            unit = None
        if not  unit == None:
            hists = [_dataHist] + _mcHist.values() + [_errorBand]
            NbinsX = hists[0].GetNbinsX()
            for hist in hists:
                for n in range(NbinsX):
                    c = hist.GetBinContent(n+1)
                    e = hist.GetBinError(n+1)
                    w = hist.GetBinWidth(n+1)
                    c_new = c/(w/unit) 
                    e_new = e/(w/unit) 
                    hist.SetBinContent(n+1,c_new)
                    hist.SetBinError(n+1,e_new)

        # re-scale xRange and yRange
        
        sumHist = HistogramsHandler().SumHist(_mcHist)
        isLogy = self.plot_info['LogScale'] if 'LogScale' in self.plot_info else False

        if not 'xRange' in self.plot_info or self.plot_info['xRange'] == None:
            self.plot_info['xRange'] = self.GetXRange(_dataHist)
        else:
            self.plot_info['xRange'] = tuple(self.plot_info['xRange'])

        if not 'yRange' in self.plot_info or self.plot_info['yRange'] == None:
            self.plot_info['yRange'] = self.GetYRange(_dataHist,sumHist,isLogy)
        else:
            self.plot_info['yRange'] = tuple(self.plot_info['yRange'])

        xRange = self.plot_info['xRange']
        yRange = self.plot_info['yRange']
        hists = [_dataHist] + _mcHist.values() + [_errorBand]

        for hist in hists:
            #hist.GetXaxis().SetRangeUser(*xRange)
            pass
            
        return _dataHist,_mcHist,_errorBand

    def GetXRange(self,hist):
        nBins = hist.GetNbinsX()
        xMin = hist.GetBinLowEdge(1) 
        xMax = hist.GetBinLowEdge(nBins) + hist.GetBinWidth(nBins)
        xRange = (xMin,xMax)
        return xRange

    def GetYRange(self,dataHist,sumHist,isLogScale):
        ymax = max(sumHist.GetMaximum(),dataHist.GetMaximum())
        #print 'ymax',ymax
        if isLogScale:
            yRange = (1.,ymax**1.6)
        else:
            yRange = (0.,ymax*1.6)
        return yRange              

    def GetCanvas(self):
        canvas = R.TCanvas('canvas','canvas',800,800)
        return canvas

    def GetPadUp(self):
        args = self.plotting_style['pad-up']
        if isinstance(args['constructor'], tuple):
            pad = R.TPad(*args['constructor'])        
        else:
            pad = R.TPad(args['constructor'])
        if 'LogScale' in self.plot_info and self.plot_info['LogScale']:        
            pad.SetLogy()
        self.DecorateObject(pad, args['settings'])
        return pad

    def GetPadDown(self):
        args = self.plotting_style['pad-down']
        if isinstance(args['constructor'], tuple):
            pad = R.TPad(*args['constructor'])        
        else:
            pad = R.TPad(args['constructor'])
        self.DecorateObject(pad, args['settings'])
        return pad

    def GetTHStack(self,mcHist):
        thstack = R.THStack('','')
        for name,color in self.colorScheme[-1::-1]:
            hist = mcHist[name]            
            hist.SetFillColor(color)            
            thstack.Add(hist)
        
        self.plotting_style['stack-mc']['Xaxis']['RangeUser'] = self.plot_info['xRange']        
        self.plotting_style['stack-mc']['Yaxis']['Title'] = self.plot_info['yTitle']
        self.plotting_style['stack-mc']['Minimum'] =self.plot_info['yRange'][0]
        self.plotting_style['stack-mc']['Maximum'] =self.plot_info['yRange'][1]
        return thstack

    def DecorateObject(self,obj,opts):      
      for key,value in opts.iteritems():
        if not isinstance(value, dict):
          if not isinstance(value,tuple):
            value = (value,)
          getattr(obj,'Set{0:}'.format(key))(*value)
        else:
          getter = getattr(obj,'Get{0:}'.format(key))
          _obj = getter()
          self.DecorateObject(_obj,value)

class Painter(object):
    def __init__(self,pic_name):
        self.pic_name = pic_name

    def PrintPlot(self,objects,thstack_sets,texts):
        objects['canvas'].Draw()
        objects['pad_up'].Draw()
        objects['pad_down'].Draw()
        objects['pad_up'].cd()
        objects['hstack_mc'].Draw('HIST')        
        ObjectsHandler().DecorateObject(objects['hstack_mc'], thstack_sets)
        objects['hist_data'].Draw('esame')
        objects['legend_up'].Draw('same')
        
        for txt in texts:
            self.DrawText(*txt)

        objects['pad_down'].cd()
        objects['hist_ratio'].Draw('same')
        objects['hist_ratio_band'].Draw('E2same')
        objects['legend_down'].Draw('same')        

        objects['canvas'].Print(self.pic_name)   

    def DrawText(self,text,x,y,color=1,tsize=-1):
        l = R.TLatex()
        if tsize>0:
          l.SetTextSize(tsize)
        l.SetNDC()
        l.SetTextColor(color)
        l.DrawLatex(x,y,text)        