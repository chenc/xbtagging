import os,re,json
import AtlasStyle
from Drawer_ControlPlots import DrawAPI

import ROOT as R

R.gROOT.SetBatch(1)

import socket
RunOnUI =  socket.gethostname().startswith('ui')


def GetErrorsSyst():
    
    #ttbar_1ptag2jet_cx_TP_MVA100_MV2c10_FixedCut_MET_SysRadLow
    errors = [
        ['Single top',         ['stopWt_DR',           'RadHigh',      inputPath + '/hist-stopWt_DR.root'],    ],
        ['Single top',         ['stopWt_DR',           'RadLow',       inputPath + '/hist-stopWt_DR.root'],    ],
        ['t#bar{t}',           ['ttbar',               'RadLow',       inputPath + '/hist-ttbar.root'],   ],
    ]

    #ttbar_1ptag2jet_cx_TP_MVA100_MV2c10_FixedCut_MET_SysPDF4LHC_11;1
    #ttbar PDF
    for n in range(30):
        errors.append(['t#bar{t}',    ['ttbar',   'PDF4LHC_0',   inputPath + '/hist-ttbar.root'],   ['ttbar',  'PDF4LHC_%s'%(n+1),   inputPath + '/hist-ttbar.root'] ])
    
    #stop PDF
    for n in range(30):
        errors.append(['Single top',   ['stopWt_DR', 'PDF4LHC_0',   inputPath + '/hist-stopWt_DR.root'],     ['stopWt_DR', 'PDF4LHC_%s'%(n+1),   inputPath + '/hist-stopWt_DR.root'] ])

    return errors


#ttbar ME/Shower, stop DR/DS
def GetErrorsTHEORY():
    errors = [
        # sample                      key1                 key2                    file
        ['t#bar{t}',           ['ttbar_aMcAtNloPy8',        None,          inputPath + '/hist-ttbar_aMcAtNloPy8.root'],      ],
        ['t#bar{t}',           ['ttbar_PowhegHerwig7',      None,          inputPath + '/hist-ttbar_PowhegHerwig7.root'],    ],
        ['t#bar{t}',           ['ttbar_radHi',              None,          inputPath + '/hist-ttbar_radHi.root'],               ],
        ['Single top',         ['stopWt_DS',                None,          inputPath + '/hist-stopWt_DS.root'],              ],

    ]
    return errors


def GetSamples():    
        samples = {
            'Data' : [
                ['Data','data',inputPath + '/hist-data15.root'],
                ['Data','data',inputPath + '/hist-data16.root'],
            ],
            'MC' : [
                ['t#bar{t}',       R.kWhite,      'ttbar' ,                 None,    inputPath + '/hist-ttbar.root'],            
                ['Single top',     R.kRed,        'stopWt_DR',                 None,    inputPath + '/hist-stopWt_DR.root'],
                ['z+jets',         R.kBlue,       'Ztt_sh221',                      None,    inputPath + '/hist-Ztt_sh221.root'],  
                ['Diboson',        R.kYellow,     'VV_Sh222',   None,    inputPath + '/hist-VV_Sh222.root'],
                ['Mis. leptons',   R.kGreen,      'fake',                   None,    inputPath + '/hist-data15.root'],
                ['Mis. leptons',   R.kGreen,      'fake',                   None,    inputPath + '/hist-data16.root'],    
            ],
            # 'bb','bc','bl','bx','cc','cl','cx','ll','lx'
        }


        return samples


def GetTexts(category,variable):
    text1 = 'Category Info Not Defined!'
    # if 'PxT' in variable:
    #     text1 = 'Probe jets'
    # elif 'CalJet' in variable:
    #     text1 = 'Pre tag jets'
    # elif 'BDT' in variable:
    #     text1 = 'BDT score'
    # elif variable in ['n_fjets', 'pt_jet0', 'pt_lep0', 'met', 'minl1j','pt_jet1', 'm_lj']:
    #     text1 = 'BDT input variables'
    # elif variable in ['Mll','MET','averageMu','actualMu','averageMuScaled','actualMuScaled','Ntagged'] and '1ptag2jet' in category:
    #     text1 = '>= 1 tagged events'
    # elif variable in ['Mll','MET','averageMu','actualMu','averageMuScaled','actualMuScaled'] and '0ptag2jet' in category:
    #     text1 = 'Pre tag events'

    # now we play this text defining in a new way
    if category == '0ptag2jet_TP':
        if 'CalJet' in variable: 
            text1 = 'Jets in Pre-tag 2 Jets Events'
        else:
            text1 = 'Pre-tag 2 Jets Events'
    elif category == '1ptag2jet_TP':
        if 'CalJet' in variable: 
            text1 = 'Jets in 1+ Tagged Events'
        elif 'BDT' in variable:
            text1 = 'Purity BDT score'
        elif variable in ['n_fjets', 'pt_jet0', 'pt_lep0', 'met', 'minl1j','pt_jet1', 'm_lj']:
            text1 = 'Purity BDT input'
        else: text1 = '1+ Tagged Events'
    elif category == '1ptag2jet_TP_MVA100_MV2c10_FixedCut':
        if 'CalJet' in variable: 
            text1 = 'Jets w/o Purity BDT cut'
        else: 
            text1 = 'w/o Purity BDT cut'
    elif category == '1ptag2jet_TP_MVA100_MV2c10_FixedCut_PxT85':
        text1 = 'Probe jets w/o Purity BDT cut'
    elif category == '1ptag2jet_TP_MVA80_MV2c10_FixedCut':
        if 'CalJet' in variable: 
            text1 = 'Jets w/ Purity BDT cut @ 80'
        else: 
            text1 = 'Purity BDT cut @ 80'
    elif category == '1ptag2jet_TP_MVA80_MV2c10_FixedCut_PxT85':
        text1 = 'Probe jets w/ Purity BDT cut @ 80'

    texts = [
        ['#font[72]{ATLAS}',0.2,0.844,1,0.05*1.58],
        ['#font[42]{Internal}',0.37, 0.844,1,0.05*1.58],
        ['#font[42]{#sqrt{s}=13 TeV, 36.1 fb^{-1}}',0.2, 0.77, 1, 0.0375*1.58],
        ['#font[42]{%s}'%(text1),0.2, 0.7, 1, 0.03*1.58],
    ]
    return texts


def GetPlot_info(variable):
    plot_info = {
        'xRange':None,#[0,600],
        'xBins': None,#[0,20,30,40,60,85,110,140,175,250,600],
        'yRange':None,        
    }
    if 'JetPt' in variable:
        info = {
            'unit':10.,
            'LogScale':True,
            'xTitle':'p_{T}(jet) [GeV]',
            'yTitle':'Jets / 10 GeV',
        }
        
    elif 'JetEta' in variable:
        info = {
            'xRange':[-3.,3.],
            'xBins':[-3+0.5*n for n in range(13)],
            'unit':0.5, 
            'xTitle':'#eta(jet)',
            'yTitle':'Jets / 0.5',
        }
        
    elif 'JetMV2c10' in variable:
        info = {
            'unit':0.1,
            'LogScale':True,
            'xTitle':'MV2c10',
            'yTitle':'Jets / 0.1',    
        }
    elif 'Mll' in variable:
        info = {
            'xRange':[0.,500.],
            'xBins':[20.*n for n in range(26)],
            'unit':20.,         
            'xTitle':'m_{ll} [GeV]',
            'yTitle':'Events / 20 GeV',
        }
    elif 'Ntagged' in variable:
        info = {
            'xRang':[0.,3.],
            'xTitle':'number of tagged jets',
            'yTitle':'Events', 
        }
    # elif 'actualMu' in variable:
    #     info = {
    #         'xRange':[0.,80.],
    #         'xBins':[1.*n for n in range(81)],
    #         'unit':1.,         
    #         'xTitle':'actualMu',
    #         'yTitle':'Events',
    #     }
    # elif 'averageMu' in variable:
    #     info = {
    #         'xRange':[0.,80.],
    #         'xBins':[1.*n for n in range(81)],
    #         'unit':1.,         
    #         'xTitle':'averageMu',
    #         'yTitle':'Events',
    #     }
    elif 'averageMuScaled' in variable:
        info = {
            'xRange':[0.,50.],
            'xBins':[1.*n for n in range(51)],
            'unit':1.,         
            'xTitle':'averageMuScaled',
            'yTitle':'Events',
        }
    # elif 'actualMuScaled' in variable:
    #     info = {
    #         'xRange':[0.,80.],
    #         'xBins':[1.*n for n in range(81)],
    #         'unit':1.,         
    #         'xTitle':'actualMuScaled',
    #         'yTitle':'Events',
    #     }
    elif 'MET' in variable:
        info = {
            'xBins':[10.*n for n in range(31)],
            'xTitle':'E_{T}^{miss} [GeV]',
            'yTitle':'Entries / 10 GeV',
        }
    elif 'n_fjets' in variable:
        info = {
            'xBins':[n*1. for n in range(8)], 
            'xTitle' : 'Num of Fjets',
            'yTitle' : 'Entries',
        }
    elif 'pt_jet0' in variable:
        info = {
            'xBins':[10.*n for n in range(61)], 
            'xTitle':'p_{T}(jet0) [GeV]',
            'yTitle':'Entries / 10 GeV',            
        }        
    elif 'pt_lep0' in variable:
        info = {
            'xBins':[10.*n for n in range(31)], 
            'xTitle':'p_{T}(lep0) [GeV]',
            'yTitle':'Entries / 10 GeV',
        }
    elif 'met' in variable:
        info = {
            'xBins':[10.*n for n in range(31)], 
            'xTitle':'E_{T}^{miss} [GeV]',
            'yTitle':'Entries / 10 GeV',            
        }
    elif 'minl1j' in variable:
        info = {
            'xBins':[0.2*n for n in range(26)], 
            'xTitle':'Min#Delta R(l0,j)}',
            'yTitle':'Entries / 0.2',
        }

    elif 'pt_jet1' in variable:
        info = {
            'xBins':[10.*n for n in range(61)], 
            'xTitle':'p_{T}(jet1) [GeV]',
            'yTitle':'Entries / 10 GeV',
        }
    elif 'm_lj' in variable:
        info = {
            'xBins':[10.*n for n in range(31)], 
            'xTitle':'m_{lj}',
            'yTitle':'Entries / 10 GeV',
        }
    elif 'BDT' in variable :
        info = {
            'xRange':[-0.65,0.45],
            'xBins':[-0.65+n*0.05 for n in range(23)],
            'unit':0.1,        
            'xTitle' : 'D_{bb}^{T&P}',
            'yTitle' : 'Events / 0.1',
        }
    else:
        raise RuntimeError(category+variable)
    plot_info.update(info)
    return plot_info

def GetControlPlotsDemo(category,variable,flavComp=0): # flavComp = 0(no flav)/1(jet flav)/2(event flav)

    samples = GetSamples()
    texts = GetTexts(category,variable)

    plot_info = GetPlot_info(variable)

    


    dataFormat = '{0:}_%s_%s'%(category,variable)
    mcFormat   = '{0:}_%s_%s'%(category,variable)

    mcFormatSys = 'Sys{1:}/{0:}_%s_%s_Sys{1:}'%(category,variable)
    
    name = 'C_%s_%s'%(category,variable)

    

    errorsSyst = GetErrorsSyst()
    errorsTHEO = GetErrorsTHEORY()

    errors = {
        'modelling':[
            [errorsSyst,mcFormatSys],
            [errorsTHEO,mcFormat],
        ],        
    }

    DrawPlot(name, samples, plot_info, texts, outputPath, dataFormat, mcFormat,errors,flavComp)



def DrawPlot(pic_name,samples,plot_info,texts,outputPath,dataFormat,mcFormat,errors,flavComp):
    drawAPI = DrawAPI(outputPath)

    for entry in samples['Data']:
        drawAPI.AddData(*entry)

    for entry in samples['MC']:
        drawAPI.AddMCNominal(*entry)

    for name,items in errors.iteritems():
        drawAPI.BookErrorBand(name,items)

    drawAPI.SetPlot(pic_name, dataFormat, mcFormat,texts,plot_info)
    hist_errorBand,name_errorBand=drawAPI.Draw()
    if flavComp == 1:
        drawAPI.DrawTMD(hist_errorBand,name_errorBand)
    elif flavComp == 2:
        drawAPI.DrawNMD(hist_errorBand,name_errorBand)


def main():
    #pretag event info
    category = '0ptag2jet_TP'
    variables = [
    'Mll','MET','averageMuScaled','pt_jet0','pt_jet1','pt_lep0',
    'CalJetPt','CalJetEta','CalJetMV2c10',
    ]
    for variable in variables:
        GetControlPlotsDemo(category, variable)

    # mva variables
    category = '1ptag2jet_TP'
    variables = [
    'BDT',
    'n_fjets', 'pt_jet0', 'pt_lep0', 'met', 'minl1j','pt_jet1', 'm_lj', # seven vairables
    ]
    for variable in variables:
        GetControlPlotsDemo(category, variable, 1)

    #1ptag event info
    category = '1ptag2jet_TP_MVA100_MV2c10_FixedCut'
    variables = [
    'Mll','MET','averageMuScaled','Ntagged',
    'CalJetPt','CalJetEta','CalJetMV2c10', # 2jets
    ]
    for variable in variables:
        GetControlPlotsDemo(category, variable)

    # 1ptag probe jet info
    category = '1ptag2jet_TP_MVA100_MV2c10_FixedCut_PxT85'
    variables = [
    'CalJetPt','CalJetEta','CalJetMV2c10', # 2jets
    ]
    for variable in variables:
        GetControlPlotsDemo(category, variable, 2)

    #1ptag event info
    category = '1ptag2jet_TP_MVA80_MV2c10_FixedCut'
    variables = [
    'Ntagged',
    'Mll', 'MET','averageMuScaled','Ntagged',
    'CalJetPt','CalJetEta','CalJetMV2c10', # 2jets
    ]
    for variable in variables:
        GetControlPlotsDemo(category, variable)

    #1ptag probe jet info
    category = '1ptag2jet_TP_MVA80_MV2c10_FixedCut_PxT85'
    variables = [
    'CalJetPt','CalJetEta','CalJetMV2c10', # 2jets
    ]
    for variable in variables:
        GetControlPlotsDemo(category, variable, 2)


if __name__ == '__main__':
    # if work on ui
    if RunOnUI:
        inputPath = '/home/ichen/CxAODFWs/FTAG_awesome/run/ReaderOutput/Reader_CxAOD_FTAG2_2L_mc16a_condor_1.1/hadd/samples'
    else:
        inputPath = '../inputs/20190118_Ntagged/mc16a/'
    outputPath = './plots_mc16a_test'
    if not os.path.isdir(outputPath):
        os.system('mkdir %s'%(outputPath))
    main()
