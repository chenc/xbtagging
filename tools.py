import re,json

def split_systs(syst_list,outputFile):
    with open(syst_list,'r') as f:
        lines = f.readlines()
    systs = {}
    for line in lines:
        line = line[:-1]
        syst,side = re.findall('Sys(.*)__1(up|down)', line)[0]
        if not syst in systs:
            systs[syst] = [side]
        else:
            systs[syst].append(side)

    systs_out = {'twoSide':[],'oneSide':[]}        

    for syst,sides in systs.iteritems():
        if len(sides)==2:
            systs_out['twoSide'].append(syst)
        else:
            systs_out['oneSide'].append(syst)


    systs_out['twoSide'].sort()
    systs_out_json = json.dumps(systs_out,indent=4)

    with open(outputFile,'w') as f:
        f.write(systs_out_json)

def main():
    split_systs('dirs.txt','systs_v1.json')

if __name__ == '__main__':
    main()