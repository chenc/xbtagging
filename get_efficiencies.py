from RetrieveEfficiency import RetrieveEfficiency,RetrieveRawHist
from copy import deepcopy,copy
import toolkit
import json,os

version = 'default'

class RawHandler(RetrieveRawHist):
    def fillRawHist(self,formatNominal,formatSyst,syst_json,period):        
        #samples
        
        self.retrieveSample('tt', 'PwPy8', ['ttbar'], formatNominal)
        self.retrieveSample('tt', 'RadHigh', ['ttbar_radHi'], formatNominal)
        self.retrieveSample('tt', 'PowhegHerwig', ['ttbar_PowhegHerwig7'], formatNominal)
        #self.retrieveSample('tt', 'aMcAtNloPy8', ['ttbar_aMcAtNloPy8'], formatNominal)
        self.retrieveSample('tt', 'aMcAtNloPy8', ['ttbar_aMcAtNloPy8_noShWe'], formatNominal)

        '''
        if 'Data1516' == period:
            self.retrieveSample('tt', 'aMcAtNloPy8', ['ttbar_aMcAtNloPy8'], formatNominal)
        elif 'Data17' == period:
            self.retrieveSample('tt', 'aMcAtNloPy8', ['ttbar_aMcAtNloPy8_noShWe'], formatNominal)
        elif 'Data151617' == period:
            self.retrieveSample('tt', 'aMcAtNloPy8', ['ttbar_aMcAtNloPy8_noShWe','ttbar_aMcAtNloPy8'], formatNominal)
        '''

        self.retrieveSample('tt', 'sherpa', ['ttbar_sherpa'], formatNominal)
        self.retrieveSample('stop', 'DR', ['stopWt_DR'], formatNominal)
        self.retrieveSample('stop', 'DS', ['stopWt_DS'], formatNominal)
        self.retrieveSample('diboson', 'Sh222', ['VV_Sh222'], formatNominal)
        self.retrieveSample('diboson', 'PwPy8', ['VV_PwPy8'], formatNominal)
        self.retrieveSample('zjets', 'sh221', ['Ztt_sh221'], formatNominal)
        self.retrieveSample('zjets', 'Mad', ['Ztt_Mad'], formatNominal)
        self.retrieveSample('fake', 'nominal', ['fake'], formatNominal)
        self.retrieveSample('data', 'nominal', ['data'], formatNominal)


        #PDF variations
        
        for n in range(31):
            sysName = 'PDF4LHC_%s'%(n)
            sysFormat = formatSyst.format(sys=sysName)
            self.retrieveSample('tt', sysName, ['ttbar'], sysFormat)
            self.retrieveSample('stop', sysName, ['stopWt_DR'], sysFormat)

        #Radiations
        sysName = 'RadHigh'
        sysFormat =formatSyst.format(sys=sysName)
        self.retrieveSample('stop', sysName, ['stopWt_DR'], sysFormat)

        sysName = 'RadLow'
        sysFormat = formatSyst.format(sys=sysName)
        self.retrieveSample('stop', sysName, ['stopWt_DR'], sysFormat)
        self.retrieveSample('tt', sysName, ['ttbar'], sysFormat)

        systs = json.load(open(syst_json,'read'))

        #one side systs
        for item in systs['oneSide']:
            sysName = '%s__1up'%(item)
            sysFormat = formatSyst.format(sys=sysName)
            self.retrieveSample('stop',sysName,['stopWt_DR'],sysFormat)
            self.retrieveSample('tt',sysName,['ttbar'],sysFormat)

        #two side systs
        for item in systs['twoSide']:
            for side in ['1up','1down']:
                sysName = '%s__%s'%(item,side)
                sysFormat = formatSyst.format(sys=sysName)
                self.retrieveSample('stop',sysName,['stopWt_DR'],sysFormat)
                self.retrieveSample('tt',sysName,['ttbar'],sysFormat)

class EfficiencyHandler(RetrieveEfficiency):
    def fill_result(self,syst_json,period):
        syst_config = json.load(open(syst_json,'r'))

        result = {}
        slot_map_nominal = {
            'tt' : 'PwPy8',
            'stop' : 'DR',
            'diboson' : 'Sh222',
            #'zjets' : 'sh221',
            'zjets' : 'Mad',
            'fake' : 'nominal',
            'data' : 'nominal'            
        }
        self.slot_map_nominal = slot_map_nominal

        raw_entry_nominal = self.getRawEntry(slot_map_nominal)
        medium_entry_nominal = self.getMediumEntry(raw_entry_nominal)

        #nominal res.
        result['nominal'] = self.CalculateEfficiencies(medium_entry_nominal)


        #stat.                
        result['data stats'], result['mc stats'] = self.CalculateStat(medium_entry_nominal)

        #modellings.
        result['tt HardScatter'] = self.GetDiffError({'tt':'aMcAtNloPy8'})        
        result['tt Fragmentation'] = self.GetDiffError({'tt':'PowhegHerwig'})        
        result['tt Radiation High'] = self.GetDiffError({'tt':'RadHigh'})
        result['tt Radiation Low'] = self.GetDiffError({'tt':'RadLow'})
        result['stop DRDS'] = self.GetDiffError({'stop':'DS'})        
        result['stop Radiation High'] = self.GetDiffError({'stop':'RadHigh'})        
        result['stop Radiation Low'] = self.GetDiffError({'stop':'RadLow'})
        result['diboson Modelling'] = self.GetDiffError({'diboson':'PwPy8'})
        #result['zjets Modelling'] = self.GetDiffError({'zjets':'Mad'})
        result['zjets Modelling'] = self.GetDiffError({'zjets':'Mad'})


        for n in range(1,31):
            result['tt PDF Error %s'%(n)] = self.GetDiffError({'tt':'PDF4LHC_0'},{'tt':'PDF4LHC_%s'%(n)})
            result['stop PDF Error %s'%(n)] = self.GetDiffError({'stop':'PDF4LHC_0'},{'stop':'PDF4LHC_%s'%(n)})

        #systs. one side
        for sys in syst_config['oneSide']:
            sysUp = sys + '__1up'
            result[sys] = self.GetDiffError({'tt':sysUp,'stop':sysUp})
        
        #systs. two side
        for sys in syst_config['twoSide']:
            sysUp = sys + '__1up'
            sysDown = sys + '__1down'

            result[sys] = self.GetDiffError({'tt':sysUp,'stop':sysUp},{'tt':sysDown,'stop':sysDown})

        #normalization
        result['WtScale'] = self.GetNormalization({'stop':0.5}, {'stop':1.5})
        result['dibosonScale'] = self.GetNormalization({'diboson':0.5}, {'diboson':2.})
        result['zjetsScale'] = self.GetNormalization({'zjets':0.5}, {'zjets':1.5})
        result['fakeScale'] = self.GetNormalization({'fake':0.5}, {'fake':1.5})


        mcSamples = ['tt','diboson','zjets','fake','stop']
        lumiScaleUp = dict.fromkeys(mcSamples, 1.032)
        lumiScaleDown = dict.fromkeys(mcSamples, 0.968)
        result['lumiScale'] = self.GetNormalization(lumiScaleDown,lumiScaleUp)

        self.result = result
        for key,well_entry in result.iteritems():
            pass
            #line = '   '.join(map('{0:.5f}'.format,well_entry['sf'].vals))
            #print '{0:<18} {1:}'.format(key,line)


@toolkit.TimeCalculator()
def run_rawJson(inputFile,period,tagger,taggerCut,taggerEff,mvaWP,overide_raw=True):
    syst_json = '{0:}/systs_v1.json'.format(os.path.split(os.path.realpath(__file__))[0])
    outputPath = './output.{0:}/{1:}/{2:}_{3:}_MVA{4:}/'.format(version,period,tagger,taggerCut,mvaWP)

    formatSyst = 'Sys{sys:}/{{sample:}}_1ptag2jet_TP_MVA%s_%s_%s_{{tp:}}%s_CalJetPt_Sys{sys:}'%(mvaWP,tagger,taggerCut,taggerEff)
    formatNominal = '{sample:}_1ptag2jet_TP_MVA%s_%s_%s_{tp:}%s_CalJetPt'%(mvaWP,tagger,taggerCut,taggerEff)
    rawJson = '{0:}/raw_{1:}.json'.format(outputPath,taggerEff)
    resultJson = '{0:}/result_{1:}.json'.format(outputPath,taggerEff)

    logDir = outputPath
    if not os.path.isfile(rawJson) or overide_raw:        
        rawHandler = RawHandler(inputFile,logDir)
        rawHandler.fillRawHist(formatNominal, formatSyst, syst_json, period)
        rawHandler.dumpRawHist(rawJson)

@toolkit.TimeCalculator()
def run_efficiencyJson(period,tagger,taggerCut,taggerEff,mvaWP):
    outputPath = './output.{0:}/{1:}/{2:}_{3:}_MVA{4:}/'.format(version,period,tagger,taggerCut,mvaWP)
    rawJson = '{0:}/raw_{1:}.json'.format(outputPath,taggerEff)
    resultJson = '{0:}/result_{1:}.json'.format(outputPath,taggerEff)    
    syst_json = '{0:}/systs_v1.json'.format(os.path.split(os.path.realpath(__file__))[0])
    logDir = outputPath

    effHandler = EfficiencyHandler(rawJson, logDir)    
    effHandler.fill_result(syst_json,period)
    effHandler.dump_result(resultJson)

@toolkit.TimeCalculator()
def run_stressTestJson(period,tagger,taggerCut,taggerEff,mvaWP):    
    outputPath = './    .{0:}/{1:}/{2:}_{3:}_MVA{4:}/'.format(version,period,tagger,taggerCut,mvaWP)

    rawJson = '{0:}/raw_{1:}.json'.format(outputPath,taggerEff)
    resultJson = '{0:}/stress_result_{1:}.json'.format(outputPath,taggerEff)

    logDir = outputPath

    stressHandler = StressTestHandler(rawJson, logDir)    
    stressHandler.fill_result(period)
    stressHandler.dump_result(resultJson)    

class StressTestHandler(RetrieveEfficiency):
    def fill_result(self,period):
        

        result = {}
        pseudo_data = {
            'stop' : 'DR',
            'diboson' : 'Sh222',
            #'zjets' : 'sh221',
            'zjets' : 'Mad',
            'fake' : 'nominal',        
            'tt'   : 'sherpa'
        }        

        slot_map_PwPy8 = {
            'tt' : 'PwPy8',
            'stop' : 'DR',
            'diboson' : 'Sh222',
            'zjets' : 'Mad',
            #'zjets' : 'sh221',
            'fake' : 'nominal',
            'data' : pseudo_data,
        }

        slot_map_sherpa = deepcopy(slot_map_PwPy8)
        slot_map_sherpa['tt'] = 'sherpa'

        slot_map_PowhegHerwig = deepcopy(slot_map_PwPy8)
        slot_map_PowhegHerwig['tt'] = 'PowhegHerwig'

        slot_map_truth = deepcopy(slot_map_PwPy8)
        slot_map_truth['tt'] = 'sherpa'

        
        raw_entry_PwPy8 = self.getRawEntry(slot_map_PwPy8)
        medium_entry_PwPy8 = self.getMediumEntry(raw_entry_PwPy8)

        # PwPy8 efficiencies
        result['result PwPy8'] = self.CalculateEfficiencies(medium_entry_PwPy8)

        # PwPy8 stats
        result['data stats'],result['mc stats PwPy8'] = self.CalculateStat(medium_entry_PwPy8)
        

        raw_entry_sherpa = self.getRawEntry(slot_map_sherpa)
        medium_entry_sherpa = self.getMediumEntry(raw_entry_sherpa)

        # sherpa efficiencies
        result['result sherpa'] = self.CalculateEfficiencies(medium_entry_sherpa)

        # sherpa stats
        result['data stats sherpa'],result['mc stats sherpa'] = self.CalculateStat(medium_entry_sherpa)


        #truth efficiencies        
        result['result truth'] = self.getWellEntry(slot_map_truth)
        

        raw_entry_PowhegHerwig = self.getRawEntry(slot_map_PowhegHerwig)
        medium_entry_PowhegHerwig = self.getMediumEntry(raw_entry_PowhegHerwig)

        #PowhegHerwig efficiencies
        result['result PowhegHerwig'] = self.CalculateEfficiencies(medium_entry_PowhegHerwig)

        # PowhegHerwig stats
        _,result['mc stats PowhegHerwig'] = self.CalculateStat(medium_entry_PowhegHerwig)

        self.slot_map_nominal = slot_map_PwPy8

        result['modelling tt HardScatter'] = self.GetDiffError({'tt':'aMcAtNloPy8'})
        result['modelling tt Fragmentation'] = self.GetDiffError({'tt':'PowhegHerwig'})
        result['modelling tt Radiation High'] = self.GetDiffError({'tt':'RadHigh'})
        result['modelling tt Radiation Low'] = self.GetDiffError({'tt':'RadLow'})

        for n in range(1,31):
            result['modelling tt PDF Error %s'%(n)] = self.GetDiffError({'tt':'PDF4LHC_0'},{'tt':'PDF4LHC_%s'%(n)})
        self.result = result
        for key,well_entry in result.iteritems():
            pass

def main():
    inputFile = '/home/ichen/CxAODFWs/FTAG_awesome/run/ReaderOutput/Reader_CxAOD_FTAG2_2L_mc16a_condor_1.1/hadd/input.root'
    period = 'mc16a'
    taggers = ['MV2c10',]#'DL1']
    taggerCuts = ['FixedCut' ]#,'Hyb']
    taggerEffs = [77]#[60,70,77,85]
    mvaWPs = [100,80]

    for tagger in taggers:
        for taggerCut in taggerCuts:
            for taggerEff in taggerEffs:
                for mvaWP in mvaWPs:
                    #run_efficiencies(inputFile, period, tagger, taggerCut, taggerEff, mvaWP,False)
                    run_stressTest(period, tagger, taggerCut, taggerEff, mvaWP)
                    

if __name__ == '__main__':
    main()
