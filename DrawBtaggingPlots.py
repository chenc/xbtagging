from BtaggingPlots import BtaggingPlots
from copy import deepcopy
import toolkit
import json

version = 'default'

def run_comparison():    
    cfg_base = {
        'errbarName':'Stat.',
        'errbandName':'Total.',
        'xTitle':'Jet p_{T} [GeV]',
        'yTitle':'#varepsilon_{b}^{data} / #varepsilon_{b}^{MC}',
        'xBins':[20,30,40,60,85,110,140,175,250,600],
        'LegendPosition':[0.62,0.7,0.95,0.9],
        'yRange':[0.65,1.35],
        'fmt':['png'],
    }
    texts_base = {
        'ATLAS_LABEL':['#font[72]{ATLAS}',0.2, 0.85,],
        'ATLAS_STATUS':['#font[42]{Internal}', 0.2 + 0.164,0.85],
        'TAGGER_INFO':['#font[42]{MV2c10, #varepsilon_{b} = 77%, single cut}',0.2,0.30,0.03],
        'COMPARISON':['#font[42]{Data 15,16 v.s Data 17}',0.2,0.37,0.03],
        'JET_INFO':['#font[42]{Anti-k_{t} R=0.4 calorimeter jets}',0.2,0.23,0.03],
        'COM_LUMI':['#font[42]{#sqrt{s} = 13 TeV}',0.2,0.78,0.04],        
    }

    #comparison between Data18 vs [Data17,Data151617]
    jsonA = './output.Harm2_FixMuSF/Data18/MV2c10_FixedCut_MVA80/result_77.json'
    legA = 'Data18'
    for data_period in ['Data17','Data151617']:
        jsonB = './output.Harm2/{0:}/MV2c10_FixedCut_MVA80/result_77.json'.format(data_period)        
        legB = data_period
        cfg = deepcopy(cfg_base)
        texts = deepcopy(texts_base)
        texts['COMPARISON'] = ['#font[42]{Data18 v.s %s}'%(data_period),0.2,0.37,0.03]
        compare_sf('SFCom_18vs{0:}'.format(data_period[4:]), jsonA, jsonB, legA, legB, cfg, texts)

    #comparison between Previous/Current
    for data in ['18']:

        jsonA = './output.Harm2/Data{0:}/MV2c10_FixedCut_MVA80/result_77.json'.format(data)
        jsonB = './output.Harm2_FixMuSF/Data{0:}/MV2c10_FixedCut_MVA80/result_77.json'.format(data)
        legA = 'Previous'
        legB = 'Current'
        cfg = deepcopy(cfg_base)
        texts = deepcopy(texts_base)
        texts['COMPARISON'] = ['#font[42]{Previous v.s Current}',0.2,0.37,0.03]
        texts['COM_LUMI'] = ['#font[42]{#sqrt{s} = 13 TeV, %s fb^{-1}}'%(getLumi(data)),0.2,0.78,0.04]
        compare_sf('SFCom_Previous_vs_Current_%s'%(data), jsonA, jsonB, legA, legB, cfg, texts)



def compare_sf(name,jsonA,jsonB,legA,legB,cfg,texts):
    drawer = BtaggingPlots('./output.{0:}/SF_comparison/'.format(version))
    drawer.DrawSFcomparison(name, jsonA, jsonB, legA,legB, cfg, texts.values())


def getLumi(period):
    lumiMap = {
        '1516':36.1,
        '17':43.8,        
        '18':59.9,
    }
    lumi = 0.
    for key,value in lumiMap.iteritems():
        if key in period:
            lumi += value
    return lumi

@toolkit.TimeCalculator()
def run_efficiencyPlots(period,tagger,taggerCut,taggerEff,mvaWP):
    

    outputPath = './output.{0:}/{1:}/{2:}_{3:}_MVA{4:}/'.format(version,period,tagger,taggerCut,mvaWP)
    resultJson = '{0:}/result_{1:}.json'.format(outputPath,taggerEff)

    cutMap = {
        'FixedCut':'single cut',
        'Hyb':'Hybrid cut'
    }

    texts = [
        ['#font[72]{ATLAS}',0.5, 0.85,],
        ['#font[42]{Internal}', 0.664,0.85],
        ['#font[42]{{{0:}, #varepsilon_{{b}} = {1:}%, {2:}}}'.format(tagger,taggerEff,cutMap[taggerCut]),0.2,0.30,0.03],
        ['#font[42]{T&P Method}',0.2,0.37,0.03],
        ['#font[42]{Anti-k_{t} R=0.4 calorimeter jets}',0.2,0.23,0.03],
        ['#font[42]{{#sqrt{{s}} = 13 TeV, {0:} fb^{{-1}}}}'.format(getLumi(period)),0.5,0.78,0.04],
    ]

    cfg_sf = {
        'errbarName':'Stat. Uncertainty',
        'errbandName':'Total Uncertainty',
        'xTitle':'Jet p_{T} [GeV]',
        'yTitle':'#varepsilon_{b}^{data} / #varepsilon_{b}^{MC}',
        'xBins':[20,30,40,60,85,110,140,175,250,600],
        'LegendPosition':[0.6,0.2,0.93,0.35],
        'yRange':[0.6,1.3],
        'fmt':['png'],
    }

    shift = (int(taggerEff)-60)/100.
    cfg_eff = {
        'effDataName':'Data',
        'effMCName':'MC',
        'xTitle':'Jet p_{T} [GeV]',
        'yTitle':'b-jet tagging efficiency',
        'xBins':[20,30,40,60,85,110,140,175,250,600],
        'LegendPosition':[0.6,0.2,0.93,0.35],
        'yRange':[0.1+shift,1.1+shift],
        'fmt':['png'],
    }    
    drawer = BtaggingPlots(outputPath)
    drawer.DrawSF(taggerEff, resultJson, cfg_sf, texts)
    drawer.DrawEff(taggerEff, resultJson, cfg_eff, texts)




def main():
    period = 'mc16a' #mc16d
    tagger = 'MV2c10' # DL1
    taggerCut = 'FixedCut' #Hyb
    taggerEff = 77    

    run_efficiencyPlots(period, tagger, taggerCut, taggerEff, 80)
    run_efficiencyPlots(period, tagger, taggerCut, taggerEff, 100)

if __name__ == '__main__':
    main()
