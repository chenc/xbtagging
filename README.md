# XBtagging

Get the Btagging results (control plots/eff. sf. plots/breakdown tables...) from histograms

## Workflow
```mermaid
graph TD
A((root file))
A --> |DrawControlPlots| B2(control plots)
A --> |RetrieveEfficiency| B1(Json files)
B1 --> |DrawBtaggingPlots| C1(SF./Eff. plots)
B1 --> |MakeTable| C2("tables(tex/pdf/png)")
B1 --> |stress_test_plot| C3(stress test plots)
```


## Usage

### Initialize
    mkdir xBtagging  && cd xBtagging
    mkdir source run
    git clone --recurse-submodules ssh://git@gitlab.cern.ch:7999/chenc/xbtagging.git source/xbtagging
    source ./source/xbtagging/setup.sh
    cd run

### Setup each time
    cd xBtagging
    source ./source/xbtagging/setup.sh
    cd run
    

### Launch a job
Before you lanch a job, you should check this file to see if the input root file is booked correctly 

    xBtagging/source/xbtagging/runBtagging


Then Using `runBtagging --help`  to see the options for lauching a job

    runBtagging --help
      -h, --help       show this help message and exit
      --control_plots  draw control plots from root input
      --efficiency     retrieve tagging efficinecy and generate json files
      --draw_btagging  draw btagging plots from json files
      --table          get the breakdown table from json files in tex/pdf/png
                       format
      --stress_plot    get the stress test plots

For a dedicated job, using `runBtagging --<job option>` to run it. For example:
    
    runBtagging --control_plots --efficiency --draw_btagging
to draw control plots, generate results jsons and btagging plots. 

### Caveat
Be aware there is a dependency between `efficiency` and `stress_plot/table/draw_btagging` so that you have to run `efficiency` first before you launch other three jobs.








 
